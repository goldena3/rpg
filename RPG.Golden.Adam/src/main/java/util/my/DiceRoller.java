package util.my;
import java.util.Random; 

public class DiceRoller {
	private static DiceRoller instance;
	private Random rand = new Random(); 
	
	private DiceRoller() {}
	
	public static DiceRoller getInstance() {
		if(instance == null) {
			instance = new DiceRoller();
		}
		return instance;
	}
	
	public int rollDice(int numDie, int type) {
		int total = 0;
		for(int i = 0; i < numDie; i++) {
			total += rand.nextInt(type) + 1;
		}
		return total;
	}

	public int rollDice(int numDie, long type) {
		int total = 0;
		for(int i = 0; i < numDie; i++) {
			total += rand.nextInt((int)type) + 1;
		}
		return total;
	}
}

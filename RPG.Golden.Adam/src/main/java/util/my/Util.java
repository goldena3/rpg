package util.my;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import Items.Item;

public class Util {
	private static Util instance = null;
	public final int GENERICERROR = 0;
	public final int NOTENOUGHHANDS = 1;
	public final int SUCCESS = 2;
	public final int NOTENOUGHSPACE = 3;
	private Scanner kbs = new Scanner(System.in);
	private static JSONParser parser = new JSONParser();

	private Util() {}
	
	public static Util getInstance() {
		if(instance == null) {
			instance = new Util();
		}
		return instance;
	}
	
	public int searchItemArrayByName(Item[] items, String name) {
		int length = items.length;
		for(int i = 0; i < length; i++) {
			if(items[i] != null) {
				if(items[i].getName().equals(name)) {
					return i;
				}
			}
		}
		return -1;
	}
	public int countNumberOfiteminArrayWithname(Item[] items, String name) {
		int length = items.length;
		int found = 0;
		for(int i = 0; i < length; i++) {
			if(items[i] != null) {
				if(items[i].getName().equals(name)) {
					found++;
				}
			}
		}
		return found;
	}
	
	public void println(String toPrint) {
		System.out.println(toPrint);
	}

	public int calcMod(int score) {
		return (score - 10)/2;
	}
	
	public Integer[] searchItemArrayByType(Item[] items, Class<?> type){
		ArrayList<Integer> found = new ArrayList<Integer>();
		
		for(int i = 0; i < items.length; i++) {
			if(items[i] != null) {
				if(items[i].getClass() == type) {
					found.add(i);
				}
			}
		}
		return  found.toArray(new Integer[0]);
	}

	public Scanner getKbs() {
		return kbs;
	}

	public void closeScanner() {
		this.kbs.close();
	}

	public String getScannerString() {
		return kbs.nextLine();
	}

	public boolean verifyInput() {
		this.println("(respond with y/n)");
		return "y".equalsIgnoreCase(kbs.nextLine());
	}

	public int getIndexFromRange(int max) {
		if(max < 1) {
			return -1;
		}
		int selection = -1;
		do {
			this.println("(Make selection between 1 and " + max + ")");
			try {
				selection = Integer.parseInt(this.getScannerString());
			}catch(Exception e){
				this.println("Please enter a number");
			}
		} while(selection < 1 || selection > max);
		
		return selection - 1;
	}
	
	public JSONArray parseFile(String fileName) throws IOException, ParseException {
		fileName = fileName.toLowerCase();
		String file = Config.getInstance().getResourceFolder();
		switch(fileName) {
			case "buildings":
				file +=  Config.getInstance().getBuildingsFile();
				break;
			case "potions":
				file +=  Config.getInstance().getPotionfile();
				break;
			case "npc":
				file +=  Config.getInstance().getNpcFile();
				break;
			case "actions":
				file +=  Config.getInstance().getActionsFile();
				break;
			case "events":
				file +=  Config.getInstance().getEventsFile();
				break;
			case "triggers":
				file +=  Config.getInstance().getTriggersFile();
				break;
			case "eniemies":
				file +=  Config.getInstance().getEniemiesFile();
				break;
		}
		Reader reader = new FileReader(file); 

		return (JSONArray) parser.parse(reader);	
	}
	
	public JSONObject parseFileToJsonObject(String fileName)throws IOException, ParseException {
		fileName = fileName.toLowerCase();
		String file = Config.getInstance().getResourceFolder();
		switch(fileName) {
			case "map":
				file +=  Config.getInstance().getMapFile();
				break;
			case "story":
				file +=  Config.getInstance().getStoryFile();
				break;
			case "dialog":
				file +=  Config.getInstance().getDialogFile();
				break;
		}
		Reader reader = new FileReader(file); 

		return (JSONObject) parser.parse(reader);	
	}
	
	public JSONObject searchJsonArrayForName(String name, JSONObject[] array) {
		for(int i = 0; i < array.length; i++) {
			if(array[i].containsKey("name") && array[i].get("name").equals(name)) {
				return array[i];
			}
		}
		return null;
	}
	

	public int findIndexInJsonArrayOfName(String name, JSONObject[] array) {
		for(int i = 0; i < array.length; i++) {
			if(array[i].containsKey("name") && array[i].get("name").equals(name)) {
				return i;
			}
		}
		return -1;
	}

	public void printInstructionsArray(String[] commands) {
		for(int i = 0; i < commands.length; i++) {
			this.println(i+1 + (i+1 > 9 ? " | " : "  | ") + commands[i]);
		}
	}
	
	public void printInstructionsArray(List<String> commands) {
		for(int i = 0; i < commands.size(); i++) {
			this.println(i+1 + (i+1 > 9 ? " | " : "  | ") + commands.get(i));
		}
	}

}
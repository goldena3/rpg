package util.my;

import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class DialogTree {
	private DialogTreeNode root;
	private String introText;
	@Override
	public String toString() {
		return "DialogTree [root=" + root +"]";
	}

	private static Util util = Util.getInstance();
	
	public DialogTree(String name) throws IOException, ParseException {
		JSONObject jsonDialog = Util.getInstance().parseFileToJsonObject("dialog");
		System.out.println(jsonDialog);
		this.introText = (String) ((JSONObject)jsonDialog.get(name)).get("introText");
		root = new DialogTreeNode("", "");
		addOptions((JSONArray) ((JSONObject)jsonDialog.get(name)).get("options"), root);
	}
	
	private void addOptions(JSONArray options, DialogTreeNode parent) {
		for(int i = 0; options != null && i < options.size(); i++) {
			DialogTreeNode newNode;
			JSONObject curr = (JSONObject) options.get(i);
			if(curr.containsKey("exitNode")) {
				newNode = new DialogTreeNode((String) curr.get("playerText"), (String) curr.get("responseText"), (boolean) curr.get("exitNode"));
			}else {
				newNode = new DialogTreeNode((String) curr.get("playerText"), (String) curr.get("responseText"));
				if(curr.containsKey("options")) {
					addOptions((JSONArray) curr.get("options"), newNode);
				}
			}
			newNode.setParent(parent);
			parent.addDialogTreeNode(newNode);
		}
		
		
	}

	public void runDialog() {
		util.println(introText);
		DialogTreeNode currNode = root;
		while(!currNode.isExitNode()) {
			ArrayList<String> options = new ArrayList<String>();
			for(int i = 0; i < currNode.getOptions().size(); i++) {
				options.add(currNode.getOptions().get(i).getPlayerText());
			}
			options.add("Exit Dialog");
			if(!currNode.equals(root)) {
				options.add("Previous");
			}
			util.printInstructionsArray(options.toArray(new String[0]));
			int choice = util.getIndexFromRange(options.size());
			switch(options.get(choice)) {
				case "Exit Dialog":
					return;
				case "Previous":
					currNode = currNode.getParent();
					break;
				default:
					currNode = currNode.getOptions().get(choice);
			}
			if(!currNode.equals(root)) {
				util.println("You: " + currNode.getPlayerText());
				util.println("Them: " + currNode.getResponseText());
			}
			if(!currNode.isExitNode() && currNode.getOptions().size() < 1) {
				currNode = root; 
			}
		}
	}
	
	
	private class DialogTreeNode {
		private ArrayList<DialogTreeNode> options = new ArrayList<DialogTreeNode>();
		private String playerText;
		private String responseText;
		@Override
		public String toString() {
			return "DialogTreeNode [options=" + options + ", playerText=" + playerText + ", responseText=" + responseText
					+ ", exitNode=" + exitNode + "]";
		}

		private boolean exitNode = false;
		private DialogTreeNode parent = null;
		
		public DialogTreeNode(String playerText, String responseText) {
			this.setPlayerText(playerText);
			this.setResponseText(responseText);
		}
		
		public DialogTreeNode(String playerText, String responseText, boolean exitNode) {
			this.setPlayerText(playerText);
			this.setResponseText(responseText);
			this.setExitNode(exitNode);
		}
		
		public String getPlayerText() {
			return playerText;
		}
		private void setPlayerText(String playerText) {
			this.playerText = playerText;
		}
		public String getResponseText() {
			return responseText;
		}
		private void setResponseText(String responseText) {
			this.responseText = responseText;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (exitNode ? 1231 : 1237);
			result = prime * result + ((options == null) ? 0 : options.hashCode());
			result = prime * result + ((parent == null) ? 0 : parent.hashCode());
			result = prime * result + ((playerText == null) ? 0 : playerText.hashCode());
			result = prime * result + ((responseText == null) ? 0 : responseText.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DialogTreeNode other = (DialogTreeNode) obj;
			if (exitNode != other.exitNode)
				return false;
			if (options == null) {
				if (other.options != null)
					return false;
			} else if (!options.equals(other.options))
				return false;
			if (parent == null) {
				if (other.parent != null)
					return false;
			} else if (!parent.equals(other.parent))
				return false;
			if (playerText == null) {
				if (other.playerText != null)
					return false;
			} else if (!playerText.equals(other.playerText))
				return false;
			if (responseText == null) {
				if (other.responseText != null)
					return false;
			} else if (!responseText.equals(other.responseText))
				return false;
			return true;
		}

		public void addDialogTreeNode(DialogTreeNode newNode) {
			options.add(newNode);
		}

		public boolean isExitNode() {
			return exitNode;
		}

		private void setExitNode(boolean exitNode) {
			this.exitNode = exitNode;
		}

		public DialogTreeNode getParent() {
			return parent;
		}

		public void setParent(DialogTreeNode parent) {
			this.parent = parent;
		}
		
		public ArrayList<DialogTreeNode> getOptions(){
			return this.options;
		}

	}


}

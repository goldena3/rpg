package Map;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import RPG.EventConection;
import util.my.Config;


@SuppressWarnings("unchecked")
public class Map {
	private static Map instance = null;
	private static JSONParser parser = new JSONParser();
	private  Config config = Config.getInstance();
	private List<City> cities = new ArrayList<City>();
	private List<Route> routes = new ArrayList<Route>();
	private City playerLocation;
	private Building PlayerBuildingLocation;
	private Room playerRoomLocation = null;
	private String playerPositionInRoom;
	
	public List<Route> getRoutes() {
		return routes;
	}

	private Map() throws IOException, ParseException, LocationNotFoundException {
		genMap();
	}
	
	private void genMap() throws IOException, ParseException, LocationNotFoundException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getMapFile()); 
	     JSONObject jsonMap = (JSONObject) parser.parse(reader);
	     JSONArray jsonCities = (JSONArray) jsonMap.get("cities");
	     JSONArray jsonRoutes = (JSONArray) jsonMap.get("routes");
		Iterator<JSONObject> itr = jsonCities.iterator();
	     while(itr.hasNext()) {
	    	 JSONObject currCity = itr.next();
	    	 City newCity = new City((String) currCity.get("name"));
	    	 if(currCity.containsKey("description")) {
	    		 newCity.setDescription((String) currCity.get("description"));
	    	 }
	    	 newCity.genBuildings((JSONArray) currCity.get("buildings"));
	    	 cities.add(newCity);
	     }
	     itr = jsonRoutes.iterator();
	     while(itr.hasNext()) {
	    	 JSONObject currRoute = itr.next();
	    	 String[] points = (String[]) ((JSONArray) currRoute.get("points")).toArray(new String[0]);
	    	 routes.add(new Route(this.findCity(points[0]), this.findCity(points[1]), (boolean) currRoute.get("oneWay")));
	     }
	}

	

	@Override
	public String toString() {
		return "Map [cities=" + cities + ", routes=" + routes + ", playerLocation=" + playerLocation + "PlayerBuildingLocation" + PlayerBuildingLocation +"]";
	}

	private City findCity(String name) throws LocationNotFoundException{
		for(int i = 0; i < cities.size(); i++) {
			if(name.equals(cities.get(i).getName())){
				return cities.get(i);
			}
		}
		throw new LocationNotFoundException("Unable to find city " + name);
	}

	public static Map getInstance() throws IOException, ParseException, LocationNotFoundException {
		if(instance == null) {
			instance = new Map();
		}
		return instance;
	}
	
	public static void regenerateMap() throws IOException, ParseException, LocationNotFoundException {
		instance = new Map();		
	}

	public String getPlayerLocation() {
		return playerLocation.getName();
	}

	public void moveToCity(String cityName) throws LocationNotFoundException, IOException, ParseException {
		for(int i = 0; i < this.routes.size() && this.playerLocation != null; i++) {
			if(routes.get(i).getStart().getName().equals(playerLocation.getName()) &&
					routes.get(i).getEnd().getName().equals(cityName)) {
				routes.get(i).travel();
			}
		}
		this.PlayerBuildingLocation = null;
		this.playerRoomLocation = null;
		this.playerLocation = findCity(cityName);
		this.playerLocation.addItemToJournal();
	}

	public String humanReadableMap() {
		String map = "Current city location: " + playerLocation.getName() + "\n";
		if(isPlayerInBuilding()) {
			map += "Curent Building: " + PlayerBuildingLocation.getName() + "\n";
			map += "Curent Room: " + playerRoomLocation.getName() + "\n";
		}
		for(int i = 0; i < cities.size(); i++) {
			City curr = cities.get(i);
			ArrayList<Route> connectingRoutes = findRoutesFromCity(curr.getName());
			map += "Can travel from " + curr.getName() + " to:\n";
			for(int j = 0; j < connectingRoutes.size(); j++) {
				if(connectingRoutes.get(j).getStart().getName().equals(curr.getName())) {
					map += connectingRoutes.get(j).getEnd().getName() + "\n";
				}else{
					map += connectingRoutes.get(j).getStart().getName() + "\n";
				}
			}
		}
		return map;
	}

	private ArrayList<Route> findRoutesFromCity(String name) {
		ArrayList<Route> connectingRoutes = new ArrayList<Route>();
		for(int i = 0; i < routes.size(); i++) {
			if(routes.get(i).getStart().getName().equals(name) ||
				(routes.get(i).getEnd().getName().equals(name) && !routes.get(i).isOneWay())) {
				connectingRoutes.add(routes.get(i));
			}
		}
		return connectingRoutes;
	}

	public String[] getPlayerAdjacientCities() {
		String[] adjacientCities = new String[findRoutesFromCity(playerLocation.getName()).size()];
		Iterator<Route> itr = findRoutesFromCity(playerLocation.getName()).iterator();
		int i = 0;
		while(itr.hasNext()) {
			Route curr = itr.next();
			if(curr.getStart().getName().equals(playerLocation.getName())) {
				adjacientCities[i] = curr.getEnd().getName();
			}else {
				adjacientCities[i] = curr.getStart().getName();
			}
			i++;
		}
		return adjacientCities;
	}

	public Building getPlayerBuildingLocation() {
		return PlayerBuildingLocation;
	}
	
	public boolean isPlayerInBuilding() {
		return PlayerBuildingLocation != null;
	}
	
	
	
	public void moveTobuilding(String name) throws LocationNotFoundException, IOException, ParseException{
		if(name.equals("") || name == null) {
			PlayerBuildingLocation = null;
		}
		PlayerBuildingLocation = playerLocation.findbuilding(name);
		PlayerBuildingLocation.addItemToJournal();
		Room entryRoom = null;
		for(int i = 0; i < PlayerBuildingLocation.getRooms().size(); i++) {
			if(PlayerBuildingLocation.getRooms().get(i).isEntry()) {
				entryRoom = PlayerBuildingLocation.getRooms().get(i);
				break;
			}
		}
		this.playerRoomLocation = entryRoom;
		this.playerRoomLocation.addItemToJournal();
	}

	public String[] getCurrentCityBuildings() {
		ArrayList<String> buildings = new ArrayList<String>();
		Iterator<Building> itr = playerLocation.getBuildings().iterator();
		while(itr.hasNext()) {
			Building curr = itr.next();
			if(!isPlayerInBuilding() || !curr.getName().equals(PlayerBuildingLocation.getName())) {
				buildings.add(curr.getName());
			}
		}
		return buildings.toArray(new String[0]);
	}

	public String getPlayerRoomLocation() {
		return playerRoomLocation.getName();
	}

	public void moveToRoom(String roomName) throws LocationNotFoundException, IOException, ParseException {
		for(int i = 0; i < this.PlayerBuildingLocation.getRooms().size(); i++) {
			if(roomName.equals(this.PlayerBuildingLocation.getRooms().get(i).getName())) {
				this.playerRoomLocation = this.PlayerBuildingLocation.getRooms().get(i);
				this.playerRoomLocation.addItemToJournal();
				return;
			}
		}
		throw new LocationNotFoundException("Unable to Find Room " + roomName +". In building " + this.PlayerBuildingLocation.getName());
	}

	public String[] getPlayerAdjacientrooms() {
		ArrayList<String> rooms = new ArrayList<String>();
		Iterator<Room> itr = this.PlayerBuildingLocation.getRooms().iterator();
		while(itr.hasNext()) {
			Room curr = itr.next();
			if(!this.playerRoomLocation.getName().equals(curr.getName())) {
				rooms.add(curr.getName());
			}
		}
		return rooms.toArray(new String[0]);
	}
	
	public Location searchLocationArray(String name, Location[] locations) throws LocationNotFoundException {
		for(int i = 0; i < locations.length; i++ ) {
			if(name.equals(locations[i].getName())) {
				return locations[i];
			}
		}
		throw new LocationNotFoundException("Cannot find loaction with name: " + name);

	}
	
	public Location FindaLocation(String name, String type) throws Exception {
		Location returnVal = null;
		switch(type){
			case "city":
				returnVal = searchLocationArray(name, cities.toArray(new Location[0]));
				break;
			case "building":
				for(int i = 0; i < cities.size(); i++) {
					try {
						returnVal = searchLocationArray(name, cities.get(i).getBuildings().toArray(new Location[0]));
					} catch( Exception e) {
						if(i == cities.size() - 1) {
							throw new Exception(e.getMessage());
						}
					}
				}
				break;
			case "room":
				for(int i = 0; i < cities.size(); i++) {
					for(int j = 0; j < cities.get(i).getBuildings().size(); j++) {
						try {
							returnVal = searchLocationArray(name, cities.get(i).getBuildings().get(j).getRooms().toArray(new Location[0]));
						}catch(Exception e){
							if(i == cities.size() - 1 && j == cities.get(i).getBuildings().size() -1) {
								throw new Exception(e.getMessage());
							}
						}
					}
					if(returnVal != null) {
						break;
					}
				}
				break;
			default:
				throw new Exception("unknown location type: " + type);
		}
		return returnVal;
	}

	public boolean playerAtLocation(Location location) {
		if(location != null && (location.equals(playerLocation) || location.equals(PlayerBuildingLocation) || location.equals(this.playerRoomLocation))){
			return true;
		}
		return false;
	}

	public String getPlayerPositionInRoom() {
		return playerPositionInRoom;
	}

	public void setPlayerPositionInRoom(String playerPositionInRoom) {
		this.playerPositionInRoom = playerPositionInRoom;
	}

	public Route findRoute(JSONArray points) throws Exception {
		for(int i=0; i < routes.size(); i++) {
			if(routes.get(i).getStart().getName().equals((String) points.get(0)) && 
					routes.get(i).getEnd().getName().equals((String) points.get(1))) {
				return routes.get(i);
			}
		}
		throw new Exception("Unable to find route with points " + points );
	}

	public void resetRoutes() {
		this.routes.stream().forEach(route -> route.resetRoute());
	}
	
}

package Map;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Building extends Location {
	private ArrayList<Room> rooms = new ArrayList<Room>();

	public Building(String name) {
		super(name);
	}

	@SuppressWarnings("unchecked")
	public void genRooms(JSONArray jsonRooms) {
		Iterator<JSONObject> itr = jsonRooms.iterator();
		while(itr.hasNext()) {
			boolean isEntry = false;
			JSONObject currRoom = itr.next();
			if(currRoom.containsKey("entry_room") && (boolean) currRoom.get("entry_room")) {
				isEntry = true;
			}
			Room newRoom = new Room((String) currRoom.get("name"), isEntry);
			newRoom.setDescription((String) currRoom.get("description"));
			rooms.add(newRoom);
		}
	}

	@Override
	public String toString() {
		return "Building [rooms=" + rooms + ", getName()=" + getName() + ", getDescription()=" + getDescription() + "]";
	}
	
	 protected ArrayList<Room> getRooms(){
		 return this.rooms;
	 }

	@Override
	public void setName(String name) {
		this.name = name;
	}

	

}

package Map;

import java.io.IOException;

import org.json.simple.parser.ParseException;

import Character.Player;
import RPG.EventConection;
import RPG.RunRpg;

public class Route implements EventConection {
	private City start;
	private City end;
	private boolean isOneWay;
	private boolean justTraveled = false;

	public Location getStart() {
		return start;
	}

	public Location getEnd() {
		return end;
	}

	public boolean isOneWay() {
		return isOneWay;
	}

	public void setOneWay(boolean isOneWay) {
		this.isOneWay = isOneWay;
	}

	
	public Route(City s, City e, boolean oneWay) {
		this.start = s;
		this.end = e;
		this.isOneWay = oneWay;
	}

	

	@Override
	public String toString() {
		return "Route [start=" + start + ", end=" + end + ", isOneWay=" + isOneWay + ", justTraveled=" + justTraveled
				+ "]";
	}

	public boolean isJustTraveled() {
		return justTraveled;
	}

	public void travel() throws IOException, ParseException {
		this.justTraveled = true;
		this.addItemToJournal();
	}
	
	public void resetRoute() {
		this.justTraveled = false;
	}

	@Override
	public void addItemToJournal() throws IOException, ParseException {
		((Player) RunRpg.getinstance().getPlayer()).addToJournal("Moved between " + this.start.getName() + " and " + this.end.getName());
	}

	@Override
	public void setName(String name) {}
}

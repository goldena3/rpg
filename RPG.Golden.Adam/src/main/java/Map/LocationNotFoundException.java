package Map;

@SuppressWarnings("serial")
public class LocationNotFoundException extends Exception {

	public LocationNotFoundException(String string) {
		super(string);
	}

}

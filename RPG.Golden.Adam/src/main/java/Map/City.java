package Map;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import util.my.Util;

@SuppressWarnings("unchecked")
public class City extends Location {

	private ArrayList<Building> buildings = new ArrayList<Building>();
	
	public City(String name) {
		super(name);
	}

	public void genBuildings(JSONArray jsonCities) throws IOException, ParseException, LocationNotFoundException {
		Iterator<String> itr = jsonCities.iterator();
	     while(itr.hasNext()) {
	    	 Building newBuilding = new Building(itr.next());
			JSONObject building = Util.getInstance().searchJsonArrayForName(newBuilding.getName(), (JSONObject[]) Util.getInstance().parseFile("buildings").toArray(new JSONObject[0]));
	    	 if(building == null) {
	    		 throw new LocationNotFoundException("Building " + newBuilding.getName() + " not found");
	    	 }
	    	 newBuilding.setDescription((building.containsKey("description") ? (String) building.get("description") : ""));
	    	 newBuilding.genRooms((JSONArray) building.get("rooms"));
	    	 buildings.add(newBuilding);
	     }
	}

	@Override
	public String toString() {
		return "City [buildings=" + buildings + ", Name=" + getName() + ", Description=" + description+ "]";
	}

	public Building findbuilding(String name) {
		for(int i = 0; i < buildings.size(); i++) {
			if(buildings.get(i).getName().equals(name)) {
				return buildings.get(i);
			}
		}
		return null;
	}
	public ArrayList<Building> getBuildings(){
		return buildings;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}

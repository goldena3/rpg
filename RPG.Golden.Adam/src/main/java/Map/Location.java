package Map;

import java.io.IOException;

import org.json.simple.parser.ParseException;

import Character.Player;
import RPG.EventConection;
import RPG.RunRpg;

public abstract class Location implements EventConection {
	protected String name;
	protected String description;
	
	public Location(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + " [name=" + name + ", description=" + description + "]";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Boolean equals(Location location) {
		return location != null && (location.getClass().equals(this.getClass()) && location.getName().equals(this.getName()));
	}
	
	@Override
	public void addItemToJournal() throws IOException, ParseException {
		String val = "Moved to " + (this.getClass().getName().contains("City") ? "city": this.getClass().getName().contains("Building") ? "building": "room") + " " + name;
		((Player) RunRpg.getinstance().getPlayer()).addToJournal(val);
	}
}

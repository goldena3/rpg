package Map;

public class Room extends Location {
	private boolean isEntry;
	
	public Room(String name, boolean isEntry) {
		super(name);
		this.isEntry = isEntry;
	}

	public boolean isEntry() {
		return isEntry;
	}

	@Override
	public String toString() {
		return "Room [isEntry=" + isEntry + ", description=" + description + "neame=" + name + "]";
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}

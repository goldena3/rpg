package Items;

public class WeaponNotFounException extends Exception {

	public WeaponNotFounException(String msg) {
		super(msg);
	}

}

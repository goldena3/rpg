package Items;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import util.my.Config;
import util.my.Util;

public class ItemFactory {

	private static ItemFactory instance;
	private static Config config = Config.getInstance();
	private static JSONParser parser = new JSONParser();
	
	private ItemFactory() {}
	
	public static ItemFactory getInstance() {
		if(instance == null) {
			instance = new ItemFactory();
		}
		return instance;
	}
	
	private Weapon generateWeapon(String name) throws WeaponNotFounException, IOException, ParseException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getWeaponsFile());
		JSONArray weapons = (JSONArray) parser.parse(reader);
		Iterator<JSONObject> itr = weapons.iterator();
		JSONObject foundWeapon = null;
		while(itr.hasNext()) {
			JSONObject curr = itr.next();
			if(name.equals(curr.get("name"))){
				foundWeapon = curr;
			}
		}
		if(foundWeapon == null) {
			throw new WeaponNotFounException("Unable to find weapon " + name + ". In Weapons.json");
		}
		return genWeaponFromJson(foundWeapon);
	}
	
	private Weapon genWeaponFromJson(JSONObject foundWeapon) {
		Weapon weapon = new Weapon((String) foundWeapon.get("name"));
		weapon.setDamageDie((long) foundWeapon.get("damage_die"));
		weapon.setNumHands((long) foundWeapon.get("num_hands"));
		weapon.setGetBonusOnDam((boolean) foundWeapon.get("get_bonus_on_damage"));
		weapon.setWeight((long) foundWeapon.get("weight"));
		weapon.setDamStat((String) foundWeapon.get("damage_stat"));
		if(foundWeapon.containsKey("weight")) {
			weapon.setWeight((long) foundWeapon.get("weight"));
		}
		return weapon;
	}
	
	public Item genItem(JSONObject item) throws WeaponNotFounException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, IOException, ParseException, PotionNotFoundException {
		return genItem((String) item.get("type"), (String) item.get("name"));
	}

	public Item genItem(String type, String name) throws WeaponNotFounException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, IOException, ParseException, PotionNotFoundException {
		Item newItem;
		switch(type.toLowerCase()) {
			case "weapon":
				newItem = this.generateWeapon(name);
				break;
			case "armor":
			case "armour":
				newItem = this.generateArmor(name);
				break;
			case "shield":
				newItem = this.generateShield(name);
				break;
			case "potion":
				newItem = this.generatePotion(name);
				break;
			case "food":
				newItem = this.generateFood(name);
				break;
			default:
				newItem = this.generateItem(name);		
		}
		return newItem;
		
	}
	
	private Potion generatePotion(String name) throws IOException, ParseException, PotionNotFoundException {
		Potion newPotion = null;
		JSONArray armors = Util.getInstance().parseFile("potions");
		Iterator<JSONObject> itr = armors.iterator();
		JSONObject foundItem = null;
		while(itr.hasNext()) {
			JSONObject curr = itr.next();
			if(name.equals(curr.get("name"))){
				foundItem = curr;
			}
		}
		if(foundItem == null) {
			throw new PotionNotFoundException("Unable to find potion " + name + ". In " + config.getPotionfile());
		}
		newPotion = genNewPotionFromJson(foundItem);
		 
	return newPotion;
	}

	private Potion genNewPotionFromJson(JSONObject foundItem) {
		Potion newPotion = new Potion((String) foundItem.get("name"));
		newPotion.createEffects((JSONArray) foundItem.get("effects"));
		
		if(foundItem.containsKey("weight")) {
			newPotion.setWeight((long) foundItem.get("weight"));
		}
		return newPotion;
	}

	private Item generateItem(String name) throws ItemNotFoundException, IOException, ParseException {
		Item newitem = null;
		Reader reader = new FileReader(config.getResourceFolder() + config.getItemFile());
		JSONArray armors = (JSONArray) parser.parse(reader);
		Iterator<JSONObject> itr = armors.iterator();
		JSONObject foundItem = null;
		while(itr.hasNext()) {
			JSONObject curr = itr.next();
			if(name.equals(curr.get("name"))){
				foundItem = curr;
			}
		}
		if(foundItem == null) {
			throw new ItemNotFoundException("Unable to find item " + name + ". In " + config.getItemFile());
		}
		newitem = genItemFromJson(foundItem);
		 
	return newitem;
	}



	private Item genItemFromJson(JSONObject foundItem) {
		Item newItem = new Item((String) foundItem.get("name"));
		if(foundItem.containsKey("weight")) {
			newItem.setWeight((long) foundItem.get("weight"));
		}
		return newItem;
	}

	private Food generateFood(String name) throws FoodNotFoundException, IOException, ParseException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getFoodFile());
		JSONArray armors = (JSONArray) parser.parse(reader);
		Iterator<JSONObject> itr = armors.iterator();
		JSONObject foundFood = null;
			while(itr.hasNext()) {
				JSONObject curr = itr.next();
				if(name.equals(curr.get("name"))){
					foundFood = curr;
				}
			}
			if(foundFood == null) {
				throw new FoodNotFoundException("Unable to find food " + name + ". In " + config.getFoodFile());
			}
			return genFoodFromJson(foundFood);
	}
	
	private Food genFoodFromJson(JSONObject foundFood) {
		Food newfood = new Food((String) foundFood.get("name"));
		if(foundFood.containsKey("weight")) {
			newfood.setWeight((long) foundFood.get("weight"));
		}
		return newfood;

	}

	private Armor generateArmor(String name) throws ArmorNotFoundException, IOException, ParseException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getArmorFile());
		JSONArray armors = (JSONArray) parser.parse(reader);
		Iterator<JSONObject> itr = armors.iterator();
		JSONObject foundArmor = null;
		while(itr.hasNext()) {
			JSONObject curr = itr.next();
			if(name.equals(curr.get("name"))){
				foundArmor = curr;
			}
		}
			if(foundArmor == null) {
				throw new ArmorNotFoundException("Unable to find armor " + name + ". In " + config.getArmorFile());
			}
			return genArmorFromJson(foundArmor);

	}

	private Armor genArmorFromJson(JSONObject foundArmor) {
		Armor newArmor = new Armor((String) foundArmor.get("name"), (boolean) foundArmor.get("is_heavy"), (long) foundArmor.get("base"));
		if(foundArmor.containsKey("weight")) {
			newArmor.setWeight((long) foundArmor.get("weight"));
		}
		return newArmor;
	}

	private Shield generateShield(String name) {
		name += " shield";
		Shield newShield = new Shield(name);
		return newShield;
	}
}

package Items;

public class Armor extends Item {
	private int acBase;
	private boolean heavy;
	public int getAcbase() {
		return acBase;
	}
	public void setAcBase(int acBonus) {
		this.acBase = acBonus;
	}
	public boolean isHeavy() {
		return heavy;
	}
	public Armor(String n, boolean isHeavy) {
		super(n);
		this.heavy = isHeavy;
	}
	
	public Armor(String n, boolean isHeavy, long acBase) {
		super(n);
		this.heavy = isHeavy;
		this.acBase = (int) acBase;
	}
	
	public void setAcBase(long acBonus) {
		this.acBase = (int) acBonus;
	}
	@Override
	public String toString() {
		return "Armor [acBase=" + acBase + ", heavy=" + heavy + ", name=" + name + ", weight=" + weight + "]";
	}
	

}

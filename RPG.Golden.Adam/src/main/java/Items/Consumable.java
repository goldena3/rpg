package Items;

public interface Consumable {
	default public void consume() {
		System.out.println("Yum Yum Yum");
	}
}

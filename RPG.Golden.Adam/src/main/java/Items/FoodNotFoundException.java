package Items;

public class FoodNotFoundException extends Exception {

	public FoodNotFoundException(String msg) {
		super(msg);
	}

}

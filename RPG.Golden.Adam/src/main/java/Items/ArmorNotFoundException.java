package Items;

public class ArmorNotFoundException extends Exception {

	public ArmorNotFoundException(String msg) {
		super(msg);
	}

}

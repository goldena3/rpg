package Items;

public class Weapon extends Item {
	private long numHands;
	private long damageDie;
	private String damStat;
	private boolean getBonusOnDam;
	
	public Weapon(String n) {
		super(n);
	}

	public long getNumHands() {
		return numHands;
	}

	public void setNumHands(long numHands) {
		this.numHands = numHands;
	}

	public long getDamageDie() {
		return damageDie;
	}

	@Override
	public boolean equals(Object obj) {
		return super.equals(this);
	}

	public void setDamageDie(long i) {
		this.damageDie =  i;
	}

	public String getDamStat() {
		return damStat;
	}

	public void setDamStat(String damStat) {
		this.damStat = damStat;
	}

	public boolean isGetBonusOnDam() {
		return getBonusOnDam;
	}

	public void setGetBonusOnDam(boolean getBonusOnDam) {
		this.getBonusOnDam = getBonusOnDam;
	}

	@Override
	public String toString() {
		return "Weapon [numHands=" + numHands + ", damageDie=" + damageDie + ", damStat=" + damStat + ", getBonusOnDam="
				+ getBonusOnDam + ", name=" + name + ", weight=" + weight + "]";
	}
	
	
	
}

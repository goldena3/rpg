package Items;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class Potion extends Item implements Consumable {
	private ArrayList<Effect> effects;
	
	public Potion(String n) {
		super(n);
	}

	public ArrayList<Effect> getEffects() {
		return effects;
	}


	
	public void createEffects(JSONArray effects) {
		this.effects = new ArrayList<Effect>();
		Iterator<JSONObject> itr = effects.iterator();
		while(itr.hasNext()) {
			JSONObject curr = itr.next();
			Effect newEffect = new Effect();
			newEffect.setType((String) curr.get("type"));
			if((newEffect.setDieEffect((boolean) curr.get("dieEffect"))).isDieEffect()) {
				newEffect.setDieAmount((long) curr.get("die_amount"));
			}else {
				newEffect.setStaticAmount((long) curr.get("static_amount"));
			}
			this.effects.add(newEffect);
		}
		
		
	}
	
	private class Effect{
		@Override
		public String toString() {
			return "Effect [type=" + type + ", dieEffect=" + dieEffect + ", dieAmount=" + dieAmount + ", staticAmount="
					+ staticAmount + "]";
		}
		private String type;
		private boolean dieEffect;
		private int dieAmount;
		private int staticAmount;
		
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public boolean isDieEffect() {
			return dieEffect;
		}
		public Effect setDieEffect(boolean dieEffect) {
			this.dieEffect = dieEffect;
			return this;
		}
		public int getDieAmount() {
			return dieAmount;
		}
		public void setDieAmount(long dieAmount) {
			this.dieAmount = (int) dieAmount;
		}
		public int getStaticAmount() {
			return staticAmount;
		}
		public void setStaticAmount(long staticAmount) {
			this.staticAmount = (int) staticAmount;
		}
	}

	@Override
	public String toString() {
		return "Potion [effects=" + effects + "]";
	}

	

}

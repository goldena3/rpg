package Items;

public class PotionNotFoundException extends Exception {

	public PotionNotFoundException(String msg) {
		super(msg);
	}

}

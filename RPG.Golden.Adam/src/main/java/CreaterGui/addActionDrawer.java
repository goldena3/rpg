package CreaterGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class addActionDrawer extends ScreenDrawer {

	public addActionDrawer(JFrame frame) {
		super(frame);
	}

	@Override
	public void drawScreen() throws IOException, ParseException {
		JLabel namelabel = new JLabel("Action Name", JLabel.RIGHT);
		final JTextField nameInput = new JTextField(20);
		labelPanel.add(namelabel);
		fieldPanel.add(nameInput);
		JLabel baseTextlabel = new JLabel("Text on action", JLabel.RIGHT);
		final JTextField baseTextInput = new JTextField(20);
		labelPanel.add(baseTextlabel);
		fieldPanel.add(baseTextInput);
		JSONArray allCities = CreateGui.getAllCities();
		String[] citieNames = new String[allCities.size() + 1];
		citieNames[0] = "";
		for(int i = 0; i < allCities.size(); i++) {
			citieNames[i+1] = (String) ((JSONObject) allCities.get(i)).get("name");
		}
		JComboBox<String> citySelect = new JComboBox<String>(citieNames);
		final JComboBox<String> buildingSelect = new JComboBox<String>();
		final JComboBox<String> roomSelect = new JComboBox<String>();
		citySelect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					buildingSelect.removeAllItems();
					roomSelect.removeAllItems();
					String[] buildings = getBuildingsForCityIfTheyHaveRooms((String) citySelect.getSelectedItem(), allCities);
					for(int i = 0; i < buildings.length; i++) {
						buildingSelect.addItem(buildings[i]);
					}
					buildingSelect.addActionListener(new ActionListener() {
						@Override
						public void actionPerformed(ActionEvent e) {
							roomSelect.removeAllItems();
							if(!"".equals(buildingSelect.getSelectedItem())) {
								try {
									String[] rooms = CreateGui.getAllRoomsForBuilding((String) buildingSelect.getSelectedItem(), new String[0]);
									for(int i = 0; i < rooms.length; i++) {
										roomSelect.addItem(rooms[i]);
									}
								} catch (IOException | ParseException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						}
					});
					pasteScreenToFrame();
				} catch (IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		JLabel cityLabel = new JLabel("City", JLabel.RIGHT);
		labelPanel.add(cityLabel);
		fieldPanel.add(citySelect);
		JLabel buildingLabel = new JLabel("Building", JLabel.RIGHT);
		JLabel roomLabel = new JLabel("Room", JLabel.RIGHT);
		labelPanel.add(buildingLabel);
		fieldPanel.add(buildingSelect);
		labelPanel.add(roomLabel);
		fieldPanel.add(roomSelect);
		JLabel oneTimeActionLabel = new JLabel("One time Action", JLabel.RIGHT);
		JCheckBox oneTimeActionInput = new JCheckBox();
		labelPanel.add(oneTimeActionLabel);
		fieldPanel.add(oneTimeActionInput);
		JLabel isRepeatableLabel = new JLabel("Is Repeatable", JLabel.RIGHT);
		JCheckBox isRepeatabInput = new JCheckBox();
		labelPanel.add(isRepeatableLabel);
		fieldPanel.add(isRepeatabInput);
		JButton addActionButton = new JButton("Add Action");
		final Color defaultBackGround = nameInput.getBackground();
		final JLabel toplabel = new JLabel("Verifying input", JLabel.CENTER);
		addActionButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				toplabel.setText("Verifying input");
				toplabel.setBackground(Color.GREEN);
				toplabel.setOpaque(true);
				frame.getContentPane().add(toplabel, BorderLayout.NORTH);
				frame.pack(); 
				frame.repaint();
				nameInput.setBackground(defaultBackGround);
				baseTextInput.setBackground(defaultBackGround);
				citySelect.setBackground(defaultBackGround);
				if(!verifyInput()) {
					toplabel.setText("Please fix below Errors");
					toplabel.setBackground(Color.RED);
					frame.pack(); 
					frame.repaint();
				}else {
					try {
						new AddNewObject().addAction(nameInput.getText(), (buildingSelect.getSelectedItem() == null || buildingSelect.getSelectedItem().equals("") ? (String) citySelect.getSelectedItem(): (String) roomSelect.getSelectedItem()), 
								baseTextInput.getText(), isRepeatabInput.isSelected(), buildingSelect.getSelectedItem() != null && !buildingSelect.getSelectedItem().equals(""), oneTimeActionInput.isSelected());
						returnToBaseScreen();
					} catch (IOException | ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}

			private boolean verifyInput() {
				boolean val = true;
				if("".equals(nameInput.getText())) {
					nameInput.setBackground(Color.RED);
					val = false;
				}
				if("".equals(baseTextInput.getText())) {
					baseTextInput.setBackground(Color.RED);
					val = false;
				}
				if("".equals(citySelect.getSelectedItem())) {
					citySelect.setBackground(Color.RED);
					val = false;
				}
				return val;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addActionButton, BorderLayout.SOUTH);
		this.pasteScreenToFrame();
	}
	
	public String[] getBuildingsForCityIfTheyHaveRooms(String city, JSONArray allCities) throws IOException, ParseException {
		List<String> buildings = new ArrayList<>();
		buildings.add("");
		for(int i = 0; i < allCities.size(); i++) {
			if(((String)((JSONObject) allCities.get(i)).get("name")).equals(city) ) {
				JSONArray unfilteredBuildings = (JSONArray) ((JSONObject) allCities.get(i)).get("buildings");
				for(int j = 0; j < unfilteredBuildings.size(); j++) {
					if(CreateGui.getAllRoomsForBuilding((String) unfilteredBuildings.get(j), new String[0]).length > 0) {
						buildings.add((String) unfilteredBuildings.get(i));
					}
				}
				break;
			}
		}
		
		return buildings.toArray(new String[0]);
	}

}

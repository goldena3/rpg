package CreaterGui;

import java.io.IOException;

import javax.swing.JFrame;

import org.json.simple.parser.ParseException;

public class DrawScreenFactory {
	private static DrawScreenFactory instance = null;
	private JFrame frame;
	
	private DrawScreenFactory(JFrame frame) {
		this.frame = frame;
	}

	public static DrawScreenFactory getInstance(JFrame frame) {
		if( instance == null) {
			instance = new DrawScreenFactory(frame);
		}
		return instance;
	}
	
	public void drawScreen(String screenType) throws IOException, ParseException {
		ScreenDrawer drawer = null;
		switch(screenType) {
			case "addAction":
				drawer = new addActionDrawer(frame);
				break;
		}
		drawer.initScreen();
	}
}

package CreaterGui;

import java.awt.GridLayout;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.json.simple.parser.ParseException;

public abstract class ScreenDrawer {
	JFrame frame;
	JPanel labelPanel = new JPanel(new GridLayout(0, 1));
	JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
	
	public ScreenDrawer(JFrame frame) {
		this.frame = frame;
		fieldPanel.add(new JLabel());
		labelPanel.add(CreateGui.backButton);
	}
	public abstract void drawScreen() throws IOException, ParseException;
	void pasteScreenToFrame() {
		frame.pack();
		frame.repaint();
	}
	void returnToBaseScreen() {
		this.clearScreen();
		CreateGui.genBaseScreen();
	}
	void clearScreen() {
		CreateGui.clearScreen();
	}
	void initScreen() throws IOException, ParseException {
		this.clearScreen();
		this.drawScreen();
	}
}

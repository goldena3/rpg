package CreaterGui;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import util.my.Config;
import util.my.Util;

@SuppressWarnings("unchecked")
public class AddNewObject {
	private  Config config = Config.getInstance();
	private static JSONParser parser = new JSONParser();
	private Util util = Util.getInstance();
	
	public AddNewObject() {}

	public void addArmor(JTextField nameInput, JTextField weightInput, JCheckBox isheavyinput, JTextField baseInput) throws IOException, ParseException {
		JSONObject newArmor = new JSONObject();
		newArmor.put("name", 				nameInput.getText());
		newArmor.put("is_heavy", 		 	isheavyinput.isSelected());
		newArmor.put("base", 		 		Integer.parseInt(baseInput.getText()));
		if(Integer.parseInt(weightInput.getText()) != 0) {
			newArmor.put("weight", Integer.parseInt(weightInput.getText()));
		}
		Reader reader = new FileReader(config.getResourceFolder() + config.getArmorFile());
		JSONArray armors = (JSONArray) parser.parse(reader);
		armors.add(newArmor);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getArmorFile())) {
			file.write(armors.toJSONString());
	        file.flush();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}
	
	public void addWeapon(JTextField nameInput, JTextField weightInput, JComboBox<Integer> handsInput,
			JComboBox<String> damageDieInput, JCheckBox getStatOnDam, JComboBox<String> statInput,
			JCheckBox isheavyinput) throws IOException, ParseException {
		JSONObject newWeapon = new JSONObject();
		newWeapon.put("name", 				 nameInput.getText());
		newWeapon.put("num_hands", 			 handsInput.getSelectedItem());
		newWeapon.put("damage_die", 		 ((String) damageDieInput.getSelectedItem()).replace("D", ""));
		newWeapon.put("get_bonus_on_damage", getStatOnDam.isSelected());
		newWeapon.put("damage_stat", 		 statInput.getSelectedItem());
		newWeapon.put("is_heavy", 		 	isheavyinput.isSelected());
		if(Integer.parseInt(weightInput.getText()) != 0) {
			newWeapon.put("weight", Integer.parseInt(weightInput.getText()));
		}
		Reader reader = new FileReader(config.getResourceFolder() + config.getWeaponsFile());
		JSONArray weapons = (JSONArray) parser.parse(reader);
		weapons.add(newWeapon);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getWeaponsFile())) {
			file.write(weapons.toJSONString());
	        file.flush();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}

	public void addItem(JTextField nameInput, JTextField weightInput) throws IOException, ParseException {
		JSONObject newItem = new JSONObject();
		newItem.put("name", nameInput.getText());
		if(Integer.parseInt(weightInput.getText()) != 0) {
			newItem.put("weight", Integer.parseInt(weightInput.getText()));
		}
		Reader reader = new FileReader(config.getResourceFolder() + config.getItemFile());
		JSONArray weapons = (JSONArray) parser.parse(reader);
		weapons.add(newItem);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getItemFile())) {
			file.write(weapons.toJSONString());
	        file.flush();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}

	public void addFood(JTextField nameInput, JTextField weightInput) throws IOException, ParseException {
		JSONObject newItem = new JSONObject();
		newItem.put("name", nameInput.getText());
		if(Integer.parseInt(weightInput.getText()) != 0) {
			newItem.put("weight", Integer.parseInt(weightInput.getText()));
		}
		Reader reader = new FileReader(config.getResourceFolder() + config.getFoodFile());
		JSONArray weapons = (JSONArray) parser.parse(reader);
		weapons.add(newItem);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getFoodFile())) {
			file.write(weapons.toJSONString());
	        file.flush();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}

	public void addPotion(JTextField nameInput, JTextField weightInput, ArrayList<JTextField> typesArray,
			ArrayList<JCheckBox> isDieAccectsArray, ArrayList<JTextField> amountsArray) throws IOException, ParseException {
		JSONObject newPotion = new JSONObject();
		newPotion.put("name", nameInput.getText());
		if(Integer.parseInt(weightInput.getText()) != 0) {
			newPotion.put("weight", Integer.parseInt(weightInput.getText()));
		}
		if(typesArray.size() > 0) {
			JSONArray effects = new JSONArray();
			System.out.println(typesArray.size());
			for(int i = 0; i< typesArray.size(); i++) {
				if(!"".equals(typesArray.get(i).getText())){
					effects.add(new JSONObject());
					((JSONObject) effects.get(effects.size() - 1)).put("type", typesArray.get(i).getText());
					((JSONObject) effects.get(effects.size() - 1)).put("dieEffect", isDieAccectsArray.get(i).isSelected());
					if((boolean) ((JSONObject) effects.get(effects.size() - 1)).get("dieEffect")) {
						((JSONObject) effects.get(effects.size() - 1)).put("die_amount", Integer.parseInt(amountsArray.get(i).getText()));
					}else {
						((JSONObject) effects.get(effects.size() - 1)).put("static_amount", Integer.parseInt(amountsArray.get(i).getText()));
					}
				}
				
			}
			newPotion.put("effects", effects);
			
		}
		Reader reader = new FileReader(config.getResourceFolder() + config.getPotionfile());
		JSONArray potions = (JSONArray) parser.parse(reader);
		potions.add(newPotion);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getPotionfile())) {
			file.write(potions.toJSONString());
	        file.flush();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		
	}

	public void addClass(JTextField nameInput, JComboBox<String> hitDieInput, 
			JComboBox<String> armourInput, JComboBox<String> weaponInput, 
			ArrayList<JComboBox<String>> typeArray, JTextField shieldTypeInput, 
			ArrayList<JTextField> nameArray, ArrayList<JTextField> amountArray) throws IOException, ParseException {
		JSONObject newClass = new JSONObject();
		newClass.put("name", nameInput.getText());
		newClass.put("hit_die", Integer.parseInt(((String) hitDieInput.getSelectedItem()).replace("D", "")));
		JSONObject startingGear = new JSONObject();
		JSONArray equipped = new JSONArray();
		JSONObject armor = new JSONObject();
		armor.put("type", "armour");
		armor.put("name", armourInput.getSelectedItem());
		JSONObject weapon = new JSONObject();
		weapon.put("type", "weapon");
		weapon.put("name", weaponInput.getSelectedItem());
		equipped.add(armor);
		equipped.add(weapon);
		if(!"".equals(shieldTypeInput.getText())) {
			JSONObject shield = new JSONObject();
			shield.put("type", "shield");
			shield.put("name", shieldTypeInput.getText());
			equipped.add(shield);
		}
		JSONArray inInventory = new JSONArray();
		startingGear.put("equipped", equipped);
		for(int i = 0; i < typeArray.size(); i++) {
			if(!"".equals(nameArray.get(i))) {
				JSONObject item = new JSONObject();
				item.put("type", typeArray.get(i).getSelectedItem());
				item.put("name", nameArray.get(i).getText());
				if(0 != Integer.parseInt(amountArray.get(i).getText())){
					item.put("amount", Integer.parseInt(amountArray.get(i).getText()));
				}
				inInventory.add(item);
			}
		}
		startingGear.put("in_inventory", inInventory);
		newClass.put("starting_gear", startingGear);
		Reader reader = new FileReader(config.getResourceFolder() + config.getClassesFile());
		JSONArray classes = (JSONArray) parser.parse(reader);
		classes.add(newClass);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getClassesFile())) {
			file.write(classes.toJSONString());
	        file.flush();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}

	public void addCity(String cityName, String[] buildingNames, String[] Buildingdescriptions) throws IOException, ParseException  {
		JSONObject map = util.parseFileToJsonObject("map");
		JSONArray cities = (JSONArray) map.get("cities");
		JSONObject newCity = new JSONObject();
		newCity.put("name", cityName);
		JSONArray newBuildings = new JSONArray();
		for(int i = 0;i < buildingNames.length; i++) {
			if(!"".equals(buildingNames[i])) {
				newBuildings.add(buildingNames[i]);
			}
		}
		newCity.put("buildings", newBuildings);
		cities.add(newCity);
		map.put("cities", cities);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getMapFile())) {
			file.write(map.toJSONString());
	        file.flush();
	    } catch (IOException e) {
	            e.printStackTrace();
	    }
		
		JSONArray buildings = util.parseFile("buildings");
		for(int i = 0; i < buildingNames.length; i++) {
			if(!"".equals(buildingNames[i]) && util.searchJsonArrayForName(buildingNames[i], (JSONObject[]) buildings.toArray(new JSONObject[0])) == null) {
				JSONObject newBuilding = new JSONObject();
				newBuilding.put("name", buildingNames[i]);
				newBuilding.put("rooms", new JSONArray());
				if(!"".equals(Buildingdescriptions[i])) {
					newBuilding.put("description", Buildingdescriptions[i]);
				}
				buildings.add(newBuilding);
			}
		}
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getBuildingsFile())) {
			file.write(buildings.toJSONString());
	        file.flush();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	    
	}

	public void addRoomsToBuilding(String BuildingName, String[] roomNames, String[] roomDescriptions, String entryRoom) throws IOException, ParseException {
		JSONArray buildings = util.parseFile("buildings");
		JSONObject building = util.searchJsonArrayForName(BuildingName, (JSONObject[]) buildings.toArray(new JSONObject[0]));
		int index = util.findIndexInJsonArrayOfName(BuildingName, (JSONObject[]) buildings.toArray(new JSONObject[0]));
		buildings.remove(index);
		JSONArray rooms = (JSONArray) building.get("rooms");
		for(int i = 0; i < roomNames.length; i++) {
			if(!"".equals(roomNames[i]) && util.searchJsonArrayForName(roomNames[i], (JSONObject[]) rooms.toArray(new JSONObject[0])) == null) {
				JSONObject newRoom = new JSONObject();
				newRoom.put("name", roomNames[i]);
				if(!"".equals(roomDescriptions[i])) {
					newRoom.put("description", roomDescriptions[i]);
					
				}
				rooms.add(newRoom);
			}
		}
		for(int i = 0; i < rooms.size(); i++) {
			if(entryRoom.equals((String) ((JSONObject) rooms.get(i)).get("name"))) {
				((JSONObject) rooms.get(i)).put("entry_room", true);
			}else {
				((JSONObject) rooms.get(i)).put("entry_room", false);
			}
		}
		building.put("rooms", rooms);
		buildings.add(building);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getBuildingsFile())) {
			file.write(buildings.toJSONString());
	        file.flush();
	    } catch (IOException e) {
	    	e.printStackTrace();
	    }
	}

	public void addRoutesToGame(String[] startCities, String[] endCities, ArrayList<JCheckBox> isOneWay) throws IOException, ParseException {
		JSONObject map = util.parseFileToJsonObject("map");
		JSONArray routes = (JSONArray) map.get("routes");
		for(int i = 0; i < startCities.length; i++) {
			if(!routeExist(startCities[i], endCities[i], isOneWay.get(i).isSelected(), routes)) {
				JSONObject newRoute = new JSONObject();
				JSONArray points = new JSONArray();
				points .add(startCities[i]);
				points.add(endCities[i]);
				newRoute.put("points", points);
				newRoute.put("oneWay", isOneWay.get(i).isSelected());
				routes.add(newRoute);
			}
		}
		map.put("routes", routes);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getMapFile())) {
			file.write(map.toJSONString());
	        file.flush();
	    } catch (IOException e) {
	            e.printStackTrace();
	    }
	}

	private boolean routeExist(String startCity, String endCity, boolean isOneWay, JSONArray routes) {
		for(int i = 0; i < routes.size(); i++) {	
			if(startCity.equals((String) ((JSONArray)((JSONObject) routes.get(i)).get("points")).get(0)) && 
					endCity.equals((String) ((JSONArray)((JSONObject) routes.get(i)).get("points")).get(1)) && 
					isOneWay == (Boolean) ((JSONObject) routes.get(i)).get("oneWay")){
				return true;
			}
		}
		return false;
	}

	public void addAction(String name,  String locationName, String baseText, boolean isRepeatable, boolean locationIsRoom, boolean oneTimeAction) throws IOException, ParseException {
		JSONObject newAction = new JSONObject();
		newAction.put("name", name);
		newAction.put("locationName", locationName);
		newAction.put("baseText", baseText);
		newAction.put("isRepeatable", isRepeatable);
		newAction.put("locationIsRoom", locationIsRoom);
		newAction.put("oneTimeAction", oneTimeAction);
		JSONArray actions = Util.getInstance().parseFile("actions");
		actions.add(newAction);
		try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getActionsFile())) {
			file.write(actions.toJSONString());
	        file.flush();
	    } catch (IOException e) {
	            e.printStackTrace();
	    }
	}
}

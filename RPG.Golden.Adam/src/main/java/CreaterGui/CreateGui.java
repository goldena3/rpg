package CreaterGui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import util.my.Config;
import util.my.Util;

@SuppressWarnings("unchecked")
public class CreateGui {
	private static  Config config = Config.getInstance();
	private static JSONParser parser = new JSONParser();
	
	private static JFrame frame;
	public static JButton backButton = new JButton("Back");
	private static JButton mapBackButton = new JButton("Back");
	static {
		backButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearScreen();
				genBaseScreen();
			}	
		});
		mapBackButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearScreen();
				editMapScreen();
			}	
		});
		
	}

	public static void main(String[] args) {
		 JFrame.setDefaultLookAndFeelDecorated(true); 

	     frame = new JFrame("Add objects to the game"); 
	 
	     frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
	     genBaseScreen();
	     frame.setPreferredSize(new Dimension(800, 600));	     
	     frame.pack(); 
	     frame.setVisible(true); 
	}
	
	public static void genBaseScreen(){
			JPanel panel = new JPanel(new GridLayout(0, 2));
		   JButton addWeaponbutton = new JButton("Create weapon");
		   addWeaponbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					addWeaponScreen();
				}
	        });
		   JButton removeWeaponbutton = new JButton("Remove weapon");
		   removeWeaponbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					try {
						removeWeaponScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
	        });
		   JButton addArmorbutton = new JButton("Create armor");
		   addArmorbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					addArmorScreen();
				}
	        });
		   JButton removeArmorbutton = new JButton("Remove Armor");
		   removeArmorbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					try {
						removeArmorScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
	        });
		   JButton addItembutton = new JButton("Create Item");
		   addItembutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					addItemScreen();
				}
	        });
		   JButton removeItembutton = new JButton("Remove Item");
		   removeItembutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					try {
						removeItemsScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
	        });
		   JButton addFoodbutton = new JButton("Create Food");
		   addFoodbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					addFoodScreen();
				}
	        });
		   JButton removeFoodbutton = new JButton("Remove Food");
		   removeFoodbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					try {
						removeFoodScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
	        }); 
		   JButton addPotionbutton = new JButton("Create Potion");
		   addPotionbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					addPotionScreen();
				}
	        });
		   JButton removePotionbutton = new JButton("Remove Potion");
		   removePotionbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					try {
						removePotionScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
	        });
		   JButton addClassbutton = new JButton("Create Class");
		   addClassbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					try {
						addClassScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
	        });
		   JButton removeClassbutton = new JButton("Remove Class");
		   removeClassbutton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					try {
						removeClassScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
	        });
		   JButton editMapButton = new JButton("Edit Map");
		   editMapButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					editMapScreen();
				}
	        });
		   JButton addActionButton = new JButton("Add Action");
		   addActionButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						DrawScreenFactory.getInstance(frame).drawScreen("addAction");
					} catch (IOException | ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
	        });
		   panel.add(addWeaponbutton);
	        panel.add(removeWeaponbutton);
	        panel.add(addArmorbutton);
	        panel.add(removeArmorbutton);
	        panel.add(addItembutton);
	        panel.add(removeItembutton);
	        panel.add(addFoodbutton);
	        panel.add(removeFoodbutton);
	        panel.add(addPotionbutton);
	        panel.add(removePotionbutton);
	        panel.add(addClassbutton);
	        panel.add(removeClassbutton);
	        panel.add(addActionButton);
	        panel.add(editMapButton);
	        frame.getContentPane().add(panel);
	        frame.pack();
			frame.repaint();
	}
	
	
	
	

	protected static void removeClassScreen() throws IOException, ParseException {
		final JSONArray classes = getAllClasses();
		JPanel checkboxPanel = new JPanel(new GridLayout(classes.size(), 1));
		JPanel classesPanel = new JPanel(new GridLayout(classes.size(), 1));
		JCheckBox[] checkboxes = new JCheckBox[classes.size()];
		for(int i = 0; i < classes.size(); i++) {
			checkboxes[i] = new JCheckBox();
			checkboxPanel.add(checkboxes[i]);
		}
		for(int i = 0; i < classes.size(); i++) {
			JSONObject curr =((JSONObject) classes.get(i));
			classesPanel.add(new JLabel((String) curr.get("name")));
		}
		
		JButton removeClassbutton = new JButton("remove Class from game");
		removeClassbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int removed = 0;
				int size = classes.size();
				for(int i = 0; i < size; i++) {
					if(checkboxes[i].isSelected()) {
						classes.remove(i - removed);
						removed++;
					}
				}
				try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getClassesFile())) {
					file.write(classes.toJSONString());
			        file.flush();
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        }
				clearScreen();
				genBaseScreen();
			}
			
		});
		frame.getContentPane().add(checkboxPanel, BorderLayout.WEST);
		frame.getContentPane().add(classesPanel, BorderLayout.CENTER);
		frame.getContentPane().add(removeClassbutton, BorderLayout.SOUTH);
		frame.pack(); 

		frame.repaint();
	}
	
	private static JSONArray getAllClasses() throws IOException, ParseException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getClassesFile());
		return (JSONArray) parser.parse(reader);
	}

	private static void editMapScreen() {
		JPanel panel = new JPanel(new GridLayout(0, 2));
		JButton addCityButton = new JButton("Add City");
		JButton removeCityButton = new JButton("Remove Cities");
		JButton editRoomsButton = new JButton("Add rooms to Buildings");
		JButton addRoutesButton = new JButton("Add Routes");
		panel.add(backButton);
		panel.add(new JLabel("Map Options"));
		
		addCityButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearScreen();
				try {
					createCity();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
			}

			
        });
		addRoutesButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearScreen();
				try {
					createRoutes();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (ParseException e1) {
					e1.printStackTrace();
				}
			}

			
        });
		addCityButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					clearScreen();
					try {
						createCity();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}

				
	        });
		editRoomsButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearScreen();
				try {
					editBuildings();
				} catch (IOException e1) {
					
					e1.printStackTrace();
				} catch (ParseException e1) {
					
					e1.printStackTrace();
				}
			}

			
        });
		
		removeCityButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearScreen();
				try {
					removeCities();
				} catch (IOException e1) {
					
					e1.printStackTrace();
				} catch (ParseException e1) {
					
					e1.printStackTrace();
				}
			}

			
        });
		panel.add(addCityButton);
		panel.add(removeCityButton);
		panel.add(editRoomsButton);
		panel.add(addRoutesButton);
		frame.getContentPane().add(panel);
	    frame.pack();
	    frame.repaint();
	}
	
	protected static void removeCities() throws IOException, ParseException{
		JSONObject map = getmap();
		JSONArray cities = (JSONArray) map.get("cities");
		JPanel checkboxPanel = new JPanel(new GridLayout(cities.size(), 1));
		JPanel cityPanel = new JPanel(new GridLayout(cities.size(), 1));
		JCheckBox[] checkboxes = new JCheckBox[cities.size()];
		for(int i = 0; i < cities.size(); i++) {
			checkboxes[i] = new JCheckBox();
			checkboxPanel.add(checkboxes[i]);
		}
		for(int i = 0; i < cities.size(); i++) {
			JSONObject curr =((JSONObject) cities.get(i));
			cityPanel.add(new JLabel((String) curr.get("name")));
		}
		
		JButton removeCitiesButton = new JButton("Remove Selected Cities");
		removeCitiesButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearScreen();
				try {
					for(int i = 0; i < cities.size(); i++) {
						JSONArray routes = (JSONArray) map.get("routes");
						if(checkboxes[i].isSelected()) {
							removeBuildings((JSONArray) ((JSONObject) cities.get(i)).get("buildings"));
							String cityName = ((String) ((JSONObject) cities.get(i)).get("name"));
							for(int j = 0; j < routes.size(); j++) {
								JSONObject currRoute = (JSONObject) routes.get(j);
								String startCity = (String) ((JSONArray) currRoute.get("points")).get(0);
								String endCity = (String) ((JSONArray) currRoute.get("points")).get(1);
								if(cityName.equals(startCity) || cityName.equals(endCity)) {
									routes.remove(j);
									j--;
								}
							}
							cities.remove(cities.get(i));
							map.put("cities", cities);
							map.put("routes", routes);
							i--;
						}
					}
				} catch (IOException e1) {
					
					e1.printStackTrace();
				} catch (ParseException e1) {
					
					e1.printStackTrace();
				}
				try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getMapFile())) {
					file.write(map.toJSONString());
			        file.flush();
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        }
				editMapScreen();
			}

			private void removeBuildings(JSONArray buildings) throws IOException, ParseException{
				JSONArray writeBuildings = Util.getInstance().parseFile("buildings");
				for(int i = 0; i < buildings.size(); i++) {
					Iterator<JSONObject> itr = writeBuildings.iterator();
					while(itr.hasNext()) {
						JSONObject curr = itr.next();
						if(((String)buildings.get(i)).equals(curr.get("name"))) {
							writeBuildings.remove(curr);
							break;
						}
					}
				}
				try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getBuildingsFile())) {
					file.write(writeBuildings.toJSONString());
			        file.flush();
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        }
			}

			
        });
		
		frame.getContentPane().add(checkboxPanel, BorderLayout.WEST);
		frame.getContentPane().add(cityPanel, BorderLayout.CENTER);
		frame.getContentPane().add(removeCitiesButton, BorderLayout.SOUTH);
		frame.pack(); 

		frame.repaint();
		
	}

	private static JSONObject getmap() throws IOException, ParseException {
		return Util.getInstance().parseFileToJsonObject("map");
	}

	protected static void createRoutes()throws IOException, ParseException {
		JSONArray cities = getAllCities();
		ArrayList<String> cityNames = new ArrayList<String>();
		for(int i = 0; i < cities.size(); i++) {
			cityNames.add((String) ((JSONObject) cities.get(i)).get("name"));
		}
		JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
		labelPanel.add(mapBackButton);
		fieldPanel.add(new JLabel());
		
		ArrayList<JComboBox<String>> startCity = new ArrayList<JComboBox<String>>();
		ArrayList <JComboBox<String>> endCity = new ArrayList<JComboBox<String>>();
		ArrayList<JCheckBox> isOneWay = new ArrayList<JCheckBox>();
		
		JButton addRouteButton = new JButton("Add Route");
		addRouteButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				labelPanel.add(new JLabel("Start City"));
				startCity.add(new JComboBox<String>(cityNames.toArray(new String[0])));
				fieldPanel.add(startCity.get(startCity.size() - 1));
				labelPanel.add(new JLabel("End City"));
				endCity.add(new JComboBox<String>(cityNames.toArray(new String[0])));
				fieldPanel.add(endCity.get(endCity.size() - 1));
				labelPanel.add(new JLabel("One Way"));
				isOneWay.add(new JCheckBox());
				fieldPanel.add(isOneWay.get(isOneWay.size() - 1));
				frame.pack();
			    frame.repaint();
			}
		});
		
		labelPanel.add(addRouteButton);
		fieldPanel.add(new JLabel("Routes to add"));
		final Color defaultBackGround = addRouteButton.getBackground();
		final JLabel toplabel = new JLabel("Verifying input", JLabel.CENTER);
		JButton addRoutesToGame = new JButton("Add Routes To Game");
		addRoutesToGame.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				toplabel.setText("Verifying input");
				toplabel.setBackground(Color.GREEN);
				toplabel.setOpaque(true);
				frame.getContentPane().add(toplabel, BorderLayout.NORTH);
				frame.pack(); 
				frame.repaint();
				if(verifyInput()) {
					try {
						new AddNewObject().addRoutesToGame(getArrayOfStringsFromComboBox(startCity), getArrayOfStringsFromComboBox(endCity), isOneWay);
					} catch (IOException | ParseException e1) {
						e1.printStackTrace();
					}
					clearScreen();
					editMapScreen();
				}else {
					toplabel.setText("Routes cannot have the same start and end city");
					toplabel.setBackground(Color.RED);
					frame.pack(); 
					frame.repaint();
				}
			}

			private String[] getArrayOfStringsFromComboBox(ArrayList<JComboBox<String>> arraylist) {
				String[] items = new String[arraylist.size()];
				for(int i = 0; i < items.length; i++) {
					items[i] = (String) arraylist.get(i).getSelectedItem();
				}
				return items;
			}

			private boolean verifyInput() {
				boolean val = true;
				for(int i = 0; i < startCity.size(); i++) {
					if(startCity.get(i).getSelectedItem().equals(endCity.get(i).getSelectedItem())) {
						val = false;
						endCity.get(i).setBackground(Color.RED);
					}else {
						endCity.get(i).setBackground(defaultBackGround);
					}
				}
				return val;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addRoutesToGame, BorderLayout.SOUTH);
	    frame.pack();
	    frame.repaint();
	}
	
	protected static JSONArray getAllCities() throws IOException, ParseException {
		return (JSONArray) Util.getInstance().parseFileToJsonObject("map").get("cities");
	}

	protected static void editBuildings() throws IOException, ParseException{
		JSONArray buildings = Util.getInstance().parseFile("buildings");
		String[] buildingNames = new String[buildings.size()];
		for(int i = 0; i < buildings.size(); i++) {
			buildingNames[i] = (String) ((JSONObject) buildings.get(i)).get("name");
		}
		JComboBox<String> nameSelect = new JComboBox<String>(buildingNames);
		JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
		labelPanel.add(mapBackButton);
		fieldPanel.add(new JLabel());
		labelPanel.add(new JLabel("building"));
		fieldPanel.add(nameSelect);
		fieldPanel.add(new JLabel("Rooms"));

		ArrayList<JTextField> roomsNames = new ArrayList<JTextField>();
		ArrayList <JTextField> roomDescriptions = new ArrayList<JTextField>();
		
		JButton addRooms = new JButton("Add Room");
		addRooms.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				labelPanel.add(new JLabel("Name"));
				roomsNames.add(new JTextField());
				fieldPanel.add(roomsNames.get(roomsNames.size() - 1));
				labelPanel.add(new JLabel("Description"));
				roomDescriptions.add(new JTextField());
				fieldPanel.add(roomDescriptions.get(roomDescriptions.size() - 1));
				frame.pack();
			    frame.repaint();
			}
		});
		JButton addRoomsToBuilding = new JButton("Add Rooms to Building");
		addRoomsToBuilding.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearScreen();
				String[] rooms = returnValuesFromArraylistOfTextInputs(roomsNames);
				String[] descriptions = returnValuesFromArraylistOfTextInputs(roomDescriptions);
			    ArrayList<String> filteredRooms = new ArrayList<String>();
			    ArrayList<String> filteredDescriptions = new ArrayList<String>();
			    for(int i = 0; i < rooms.length; i++) {
			    	if(!"".equals(rooms[i])) {
			    		filteredRooms.add(rooms[i]);
			    		filteredDescriptions.add(descriptions[i]);
			    	}
			    }

				try {
					findEntryroom((String) nameSelect.getSelectedItem(), filteredRooms.toArray(new String[0]), filteredDescriptions.toArray(new String[0]));
				} catch (IOException | ParseException e1) {
					e1.printStackTrace();
				}
			}
		});
		labelPanel.add(addRooms);
		
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addRoomsToBuilding, BorderLayout.SOUTH);
	    frame.pack();
	    frame.repaint();
	}
	
	protected static void findEntryroom(String buildingname, String[] newRooms, String[] roomDescriptions) throws IOException, ParseException {
		JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
		JButton finishButton = new JButton("Finish");
		String[] fullListOfRooms = getAllRoomsForBuilding(buildingname, newRooms);
		JCheckBox[] isEntry = new JCheckBox[fullListOfRooms.length];
		for(int i = 0; i < fullListOfRooms.length; i++) {
			labelPanel.add(new JLabel(fullListOfRooms[i]));
			isEntry[i] = new JCheckBox();
			fieldPanel.add(isEntry[i]);
		}
		finishButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(verifyInput()) {
					try {
						clearScreen();
						new AddNewObject().addRoomsToBuilding(buildingname, newRooms, roomDescriptions, getEntryRoomName());
						editMapScreen();
					} catch (IOException | ParseException e1) {
						e1.printStackTrace();
					}
				}
			}

			private String getEntryRoomName() {
				for(int i = 0; i < fullListOfRooms.length; i++) {
					if(isEntry[i].isSelected()) {
						return fullListOfRooms[i];
					}
				}
				return null;
			}

			private boolean verifyInput() {
				boolean hasEntry = false;
				boolean multipleSelected = false;
				for(int i = 0; i < fullListOfRooms.length; i++) {
					if(isEntry[i].isSelected() && !hasEntry) {
						hasEntry = true;
					}else if(isEntry[i].isSelected() && hasEntry) {
						multipleSelected = true;
					}
				}
				return hasEntry && !multipleSelected;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(finishButton, BorderLayout.SOUTH);
	    frame.pack();
	    frame.repaint();
	}

	protected static String[] getAllRoomsForBuilding(String buildingname, String[] newRooms) throws IOException, ParseException {
		Util util = Util.getInstance();
		JSONArray alreadyAddedRooms = (JSONArray) util.searchJsonArrayForName(buildingname, (JSONObject[]) util.parseFile("buildings").toArray(new JSONObject[0])).get("rooms");
		String[] fullListOfRooms = new String[newRooms.length + alreadyAddedRooms.size()];
		for(int i = 0; i < newRooms.length; i++) {
			fullListOfRooms[i] = newRooms[i];
		}
		for(int i = newRooms.length; i < newRooms.length + alreadyAddedRooms.size(); i++) {
			fullListOfRooms[i] = (String) ((JSONObject) alreadyAddedRooms.get(i - newRooms.length)).get("name");
		}
		
		return fullListOfRooms;
	}

	protected static void createCity() throws IOException, ParseException{
		JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
		JLabel namelabel = new JLabel("name", JLabel.RIGHT);
		labelPanel.add(mapBackButton);
		labelPanel.add(namelabel);
		
		final JTextField nameInput = new JTextField(20);
		fieldPanel.add(new JLabel());
		fieldPanel.add(nameInput);
		fieldPanel.add(new JLabel("Buildings"));
		
		ArrayList<JTextField> buildingNames = new ArrayList<JTextField>();
		ArrayList <JTextField> buildingDescriptions = new ArrayList<JTextField>();
		JButton addBuildingToCitybutton = new JButton("Add Building");
		addBuildingToCitybutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				labelPanel.add(new JLabel("Name"));
				buildingNames.add(new JTextField());
				fieldPanel.add(buildingNames.get(buildingNames.size() - 1));
				labelPanel.add(new JLabel("Description"));
				buildingDescriptions.add(new JTextField());
				fieldPanel.add(buildingDescriptions.get(buildingDescriptions.size() - 1));
				frame.pack();
			    frame.repaint();
			}
		});
		labelPanel.add(addBuildingToCitybutton);
		
		JButton addCitybutton = new JButton("Add City");
		final Color defaultBackGround = nameInput.getBackground();
		final JLabel toplabel = new JLabel("Verifying input", JLabel.CENTER);
		addCitybutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				toplabel.setText("Verifying input");
				toplabel.setBackground(Color.GREEN);
				toplabel.setOpaque(true);
				frame.getContentPane().add(toplabel, BorderLayout.NORTH);
				frame.pack(); 
				frame.repaint();
				nameInput.setBackground(defaultBackGround);
				if(!verifyInput()) {
					toplabel.setText("Please fix below Errors");
					toplabel.setBackground(Color.RED);
					frame.pack(); 
					frame.repaint();
				}else {
					try {
						new AddNewObject().addCity(nameInput.getText(), returnValuesFromArraylistOfTextInputs(buildingNames), returnValuesFromArraylistOfTextInputs(buildingDescriptions));
						clearScreen();
						editMapScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
			}

			private boolean verifyInput() {
				boolean val = true;
				if("".equals(nameInput.getText())) {
					nameInput.setBackground(Color.RED);
					val = false;
				}
				return val;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addCitybutton, BorderLayout.SOUTH);
	    frame.pack();
	    frame.repaint();
	}
	
	private static String[] returnValuesFromArraylistOfTextInputs(ArrayList<JTextField> list) {
		String[] strings = new String[list.size()];
		for(int i = 0; i < list.size(); i++) {
			strings[i] = list.get(i).getText();
		}
		return strings;
	}

	protected static void addClassScreen() throws IOException, ParseException {
		String[] hitDieInputArray = {"D6", "D8", "D10", "D12"};
		JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
		JLabel namelabel = new JLabel("name", JLabel.RIGHT);
		labelPanel.add(backButton);
		labelPanel.add(namelabel);
		labelPanel.add(new JLabel("Hit die"));
		labelPanel.add(new JLabel());
		labelPanel.add(new JLabel("Armour name"));
		labelPanel.add(new JLabel("Weapon name"));
		labelPanel.add(new JLabel("Shield type"));
		final JTextField nameInput = new JTextField(20);
		final JTextField weightInput = new JTextField("0");
		final JComboBox<String> hitDieInput = new JComboBox<String>(hitDieInputArray);
		final JComboBox<String> armourInput = new JComboBox<String>(getArmourInputForDropdown());
		final JComboBox<String> weaponInput = new JComboBox<String>(getWeaponInputForDropdown());
		final JTextField shieldTypeInput = new JTextField(20);
		fieldPanel.add(new JLabel());
		fieldPanel.add(nameInput);
		fieldPanel.add(hitDieInput);
		fieldPanel.add(new JLabel("Equiped Gear", JLabel.CENTER));
		fieldPanel.add(armourInput);
		fieldPanel.add(weaponInput);
		fieldPanel.add(shieldTypeInput);
		JButton addItembutton = new JButton("Add inventory itemse");
		final Color defaultBackGround = weightInput.getBackground();
		final JLabel toplabel = new JLabel("Verifying input", JLabel.CENTER);
		addItembutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				toplabel.setText("Verifying input");
				toplabel.setBackground(Color.GREEN);
				toplabel.setOpaque(true);
				frame.getContentPane().add(toplabel, BorderLayout.NORTH);
				frame.pack(); 
				frame.repaint();
				nameInput.setBackground(defaultBackGround);
				if(!verifyInput()) {
					toplabel.setText("Please fix below Errors");
					toplabel.setBackground(Color.RED);
					frame.pack(); 
					frame.repaint();
				}else {
					try {
						clearScreen();
//						new AddNewObject().addClass(nameInput, hitDieInput);
						getInventoryitems(nameInput, hitDieInput, armourInput, weaponInput, shieldTypeInput);
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
			}

			private boolean verifyInput() {
				boolean val = true;
				if("".equals(nameInput.getText())) {
					nameInput.setBackground(Color.RED);
					val = false;
				}
				try {
					Integer.parseInt(weightInput.getText());
				}catch(Exception e) {
					weightInput.setBackground(Color.RED);
					val = false;
				}
				return val;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addItembutton, BorderLayout.SOUTH);
		frame.pack();
		frame.repaint();
		
	}

	protected static void getInventoryitems(final JTextField nameInput, final JComboBox<String> hitDieInput,
		final JComboBox<String> armourInput, final JComboBox<String> weaponInput, final JTextField shieldTypeInput) throws IOException, ParseException {
		final JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		final JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
		final JButton addItembutton = new JButton("Add Item");
		final ArrayList<JComboBox<String>> typeArray = new ArrayList<JComboBox<String>>();
		final ArrayList<JTextField> nameArray = new ArrayList<JTextField>();
		final ArrayList<JTextField> amountArray = new ArrayList<JTextField>();
		addItembutton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String[] type = {"weapon", "armor", "potion", "food", "item"};
				typeArray.add(new JComboBox<String>(type));
				nameArray.add(new JTextField());
				amountArray.add(new JTextField("0"));
				fieldPanel.add(typeArray.get(typeArray.size() - 1));
				fieldPanel.add(nameArray.get(nameArray.size() - 1));
				fieldPanel.add(amountArray.get(amountArray.size() - 1));
				labelPanel.add(new JLabel("Type"));
				labelPanel.add(new JLabel("Name"));
				labelPanel.add(new JLabel("Amount"));
				frame.pack();
				frame.repaint();
			}
			
		});
		labelPanel.add(addItembutton);
		fieldPanel.add(new JLabel());
		final JButton addClassbutton = new JButton("Add Class to Game");
		addClassbutton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(verifyInput()) {
					try {
						new AddNewObject().addClass(nameInput, hitDieInput, armourInput, weaponInput, typeArray, shieldTypeInput, nameArray, amountArray);
						clearScreen();
						genBaseScreen();
					} catch (IOException | ParseException e1) {
						
						e1.printStackTrace();
					}
				}else {
					
				}
			}

			private boolean verifyInput() {
				boolean val = true;
				for(int i = 0; i < amountArray.size(); i++) {
					if(nameArray.get(i).getText().equals("")) {
						val = false;
						nameArray.get(i).setBackground(Color.RED);
					}
					try {
						Integer.parseInt(amountArray.get(i).getText());
					} catch(Exception e) {
						val = false;
						amountArray.get(i).setBackground(Color.RED);
					}
				}
				return val;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addClassbutton, BorderLayout.SOUTH);
		frame.pack();
		frame.repaint();
	}

	

	private static String[] getWeaponInputForDropdown() throws IOException, ParseException {
		JSONArray JSON = getAllWeapons();
		String[] weapons = new String[JSON.size()];
		for(int i = 0; i < weapons.length; i++) {
			weapons[i] = (String) ((JSONObject) JSON.get(i)).get("name");
		}
		return weapons;
	}

	private static String[] getArmourInputForDropdown() throws IOException, ParseException {
		JSONArray armoursJSON = getAllArmors();
		String[] armours = new String[armoursJSON.size()];
		for(int i = 0; i < armours.length; i++) {
			armours[i] = (String) ((JSONObject) armoursJSON.get(i)).get("name");
		}
		return armours;
	}

	protected static void removePotionScreen() throws IOException, ParseException {
		final JSONArray potions = getAllPotions();
		final JCheckBox[] checkboxes = new JCheckBox[potions.size()];
		JPanel checkboxPanel = new JPanel(new GridLayout(potions.size(), 1));
		JPanel weaponsPanel = new JPanel(new GridLayout(potions.size(), 1));
		for(int i = 0; i < potions.size(); i++) {
			checkboxes[i] = new JCheckBox();
			checkboxPanel.add(checkboxes[i]);
		}
		for(int i = 0; i < potions.size(); i++) {
			JSONObject curr =((JSONObject) potions.get(i));
			weaponsPanel.add(new JLabel((String) curr.get("name")));
		}
		
		JButton removeFoodbutton = new JButton("remove food from game");
		removeFoodbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int removed = 0;
				int size = potions.size();
				for(int i = 0; i < size; i++) {
					if(checkboxes[i].isSelected()) {
						potions.remove(i - removed);
						removed++;
					}
				}
				try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getPotionfile())) {
					file.write(potions.toJSONString());
			        file.flush();
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        }
				clearScreen();
				genBaseScreen();
			}
			
		});
		frame.getContentPane().add(checkboxPanel, BorderLayout.WEST);
		frame.getContentPane().add(weaponsPanel, BorderLayout.CENTER);
		frame.getContentPane().add(removeFoodbutton, BorderLayout.SOUTH);
		frame.pack(); 

		frame.repaint();
		
	}

	private static JSONArray getAllPotions() throws IOException, ParseException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getPotionfile());
		return (JSONArray) parser.parse(reader);
	}

	protected static void addPotionScreen() {
		final JPanel labelPanel = new JPanel(new GridLayout(0, 1));
		final JPanel fieldPanel = new JPanel(new GridLayout(0, 1));
		JLabel namelabel = new JLabel("name", JLabel.RIGHT);
		JLabel weightlabel = new JLabel("Weight", JLabel.RIGHT);
		labelPanel.add(backButton);
		labelPanel.add(namelabel);
		labelPanel.add(weightlabel);
		final JTextField nameInput = new JTextField(20);
		final JTextField weightInput = new JTextField("0");
		fieldPanel.add(new JLabel());
		fieldPanel.add(nameInput);
		fieldPanel.add(weightInput);
		fieldPanel.add(new JLabel());
		JButton addEffectbutton = new JButton("Add Effect to potion");
		labelPanel.add(addEffectbutton);
		JButton addPotionbutton = new JButton("Add potion to game");
		final ArrayList<JTextField> typesArray = new ArrayList<JTextField>();
		final ArrayList<JCheckBox> isDieAccectsArray = new ArrayList<JCheckBox>();
		final ArrayList<JTextField> amountsArray = new ArrayList<JTextField>();
		addEffectbutton.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent e) {
				typesArray.add( new JTextField(20));
				isDieAccectsArray.add(new JCheckBox("", true));
				amountsArray.add(new JTextField("0"));
				fieldPanel.add(typesArray.get(typesArray.size() - 1));
				fieldPanel.add(isDieAccectsArray.get(isDieAccectsArray.size() - 1));
				fieldPanel.add(amountsArray.get(amountsArray.size() - 1));
				labelPanel.add(new JLabel("Type"));
				labelPanel.add(new JLabel("Is a die effect"));
				labelPanel.add(new JLabel("Amount"));
				frame.pack(); 
				frame.repaint();
			}
			
		});
		final Color defaultBackGround = weightInput.getBackground();
		final JLabel toplabel = new JLabel("Verifying input", JLabel.CENTER);
		addPotionbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				toplabel.setText("Verifying input");
				toplabel.setBackground(Color.GREEN);
				toplabel.setOpaque(true);
				frame.getContentPane().add(toplabel, BorderLayout.NORTH);
				frame.pack(); 
				frame.repaint();
				weightInput.setBackground(defaultBackGround);
				nameInput.setBackground(defaultBackGround);
				if(!verifyInput()) {
					toplabel.setText("Please fix below Errors");
					toplabel.setBackground(Color.RED);
					frame.pack(); 
					frame.repaint();
				}else {
					try {
						new AddNewObject().addPotion(nameInput, weightInput, typesArray, isDieAccectsArray, amountsArray);
						clearScreen();
						genBaseScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
			}

			private boolean verifyInput() {
				boolean val = true;
				if("".equals(nameInput.getText())) {
					nameInput.setBackground(Color.RED);
					val = false;
				}
				try {
					Integer.parseInt(weightInput.getText());
				}catch(Exception e) {
					weightInput.setBackground(Color.RED);
					val = false;
				}
				return val;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addPotionbutton, BorderLayout.SOUTH);
		frame.pack();
		frame.repaint();
		
	}

	protected static void removeFoodScreen() throws IOException, ParseException {
		final JSONArray foods = getAllFood();
		final JCheckBox[] checkboxes = new JCheckBox[foods.size()];
		JPanel checkboxPanel = new JPanel(new GridLayout(foods.size(), 1));
		JPanel weaponsPanel = new JPanel(new GridLayout(foods.size(), 1));
		for(int i = 0; i < foods.size(); i++) {
			checkboxes[i] = new JCheckBox();
			checkboxPanel.add(checkboxes[i]);
		}
		for(int i = 0; i < foods.size(); i++) {
			JSONObject curr =((JSONObject) foods.get(i));
			weaponsPanel.add(new JLabel((String) curr.get("name")));
		}
		
		JButton removeFoodbutton = new JButton("remove food from game");
		removeFoodbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int removed = 0;
				int size = foods.size();
				for(int i = 0; i < size; i++) {
					if(checkboxes[i].isSelected()) {
						foods.remove(i - removed);
						removed++;
					}
				}
				try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getFoodFile())) {
					file.write(foods.toJSONString());
			        file.flush();
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        }
				clearScreen();
				genBaseScreen();
			}
			
		});
		frame.getContentPane().add(checkboxPanel, BorderLayout.WEST);
		frame.getContentPane().add(weaponsPanel, BorderLayout.CENTER);
		frame.getContentPane().add(removeFoodbutton, BorderLayout.SOUTH);
		frame.pack(); 

		frame.repaint();
		
	}

	private static JSONArray getAllFood() throws IOException, ParseException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getFoodFile());
		return (JSONArray) parser.parse(reader);
	}

	protected static void addFoodScreen() {
		JPanel labelPanel = new JPanel(new GridLayout(3, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(3, 1));
		JLabel namelabel = new JLabel("name", JLabel.RIGHT);
		JLabel weightlabel = new JLabel("Weight", JLabel.RIGHT);
		labelPanel.add(backButton);
		labelPanel.add(namelabel);
		labelPanel.add(weightlabel);
		final JTextField nameInput = new JTextField(20);
		final JTextField weightInput = new JTextField("0");
		fieldPanel.add(new JLabel());
		fieldPanel.add(nameInput);
		fieldPanel.add(weightInput);
		JButton addItembutton = new JButton("Add item to game");
		final Color defaultBackGround = weightInput.getBackground();
		final JLabel toplabel = new JLabel("Verifying input", JLabel.CENTER);
		addItembutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				toplabel.setText("Verifying input");
				toplabel.setBackground(Color.GREEN);
				toplabel.setOpaque(true);
				frame.getContentPane().add(toplabel, BorderLayout.NORTH);
				frame.pack(); 
				frame.repaint();
				weightInput.setBackground(defaultBackGround);
				nameInput.setBackground(defaultBackGround);
				if(!verifyWeaponInput()) {
					toplabel.setText("Please fix below Errors");
					toplabel.setBackground(Color.RED);
					frame.pack(); 
					frame.repaint();
				}else {
					try {
						new AddNewObject().addFood(nameInput, weightInput);
						clearScreen();
						genBaseScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
			}

			private boolean verifyWeaponInput() {
				boolean val = true;
				if("".equals(nameInput.getText())) {
					nameInput.setBackground(Color.RED);
					val = false;
				}
				try {
					Integer.parseInt(weightInput.getText());
				}catch(Exception e) {
					weightInput.setBackground(Color.RED);
					val = false;
				}
				return val;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addItembutton, BorderLayout.SOUTH);
		frame.pack();
		frame.repaint();
		
	}

	protected static void removeItemsScreen() throws IOException, ParseException {
		final JSONArray items = getAllBaseItems();
		final JCheckBox[] checkboxes = new JCheckBox[items.size()];
		JPanel checkboxPanel = new JPanel(new GridLayout(items.size(), 1));
		JPanel weaponsPanel = new JPanel(new GridLayout(items.size(), 1));
		for(int i = 0; i < items.size(); i++) {
			checkboxes[i] = new JCheckBox();
			checkboxPanel.add(checkboxes[i]);
		}
		for(int i = 0; i < items.size(); i++) {
			JSONObject curr =((JSONObject) items.get(i));
			weaponsPanel.add(new JLabel((String) curr.get("name")));
		}
		
		JButton removeWeaponbutton = new JButton("remove Armors from game");
		removeWeaponbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int removed = 0;
				int size = items.size();
				for(int i = 0; i < size; i++) {
					if(checkboxes[i].isSelected()) {
						items.remove(i - removed);
						removed++;
					}
				}
				try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getItemFile())) {
					file.write(items.toJSONString());
			        file.flush();
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        }
				clearScreen();
				genBaseScreen();
			}
			
		});
		frame.getContentPane().add(checkboxPanel, BorderLayout.WEST);
		frame.getContentPane().add(weaponsPanel, BorderLayout.CENTER);
		frame.getContentPane().add(removeWeaponbutton, BorderLayout.SOUTH);
		frame.pack(); 

		frame.repaint();
		
	}

	private static JSONArray getAllBaseItems() throws IOException, ParseException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getItemFile());
		return (JSONArray) parser.parse(reader);
	}

	protected static void addItemScreen() {
		final JPanel labelPanel = new JPanel(new GridLayout(3, 1));
		final JPanel fieldPanel = new JPanel(new GridLayout(3, 1));
		final JLabel namelabel = new JLabel("name", JLabel.RIGHT);
		final JLabel weightlabel = new JLabel("Weight", JLabel.RIGHT);
		labelPanel.add(backButton);
		labelPanel.add(namelabel);
		labelPanel.add(weightlabel);
		final JTextField nameInput = new JTextField(20);
		final JTextField weightInput = new JTextField("0");
		fieldPanel.add(new JLabel());
		fieldPanel.add(nameInput);
		fieldPanel.add(weightInput);
		JButton addItembutton = new JButton("Add item to game");
		final Color defaultBackGround = weightInput.getBackground();
		final JLabel toplabel = new JLabel("Verifying input", JLabel.CENTER);
		addItembutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				toplabel.setText("Verifying input");
				toplabel.setBackground(Color.GREEN);
				toplabel.setOpaque(true);
				frame.getContentPane().add(toplabel, BorderLayout.NORTH);
				frame.pack(); 
				frame.repaint();
				weightInput.setBackground(defaultBackGround);
				nameInput.setBackground(defaultBackGround);
				if(!verifyWeaponInput()) {
					toplabel.setText("Please fix below Errors");
					toplabel.setBackground(Color.RED);
					frame.pack(); 
					frame.repaint();
				}else {
					try {
						new AddNewObject().addItem(nameInput, weightInput);
						clearScreen();
						genBaseScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
			}

			private boolean verifyWeaponInput() {
				boolean val = true;
				if("".equals(nameInput.getText())) {
					nameInput.setBackground(Color.RED);
					val = false;
				}
				try {
					Integer.parseInt(weightInput.getText());
				}catch(Exception e) {
					weightInput.setBackground(Color.RED);
					val = false;
				}
				return val;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addItembutton, BorderLayout.SOUTH);
		frame.pack();
		frame.repaint();
	}

	protected static void removeArmorScreen() throws IOException, ParseException {
		final JSONArray armors = getAllArmors();
		final JCheckBox[] checkboxes = new JCheckBox[armors.size()];
		JPanel checkboxPanel = new JPanel(new GridLayout(armors.size(), 1));
		JPanel weaponsPanel = new JPanel(new GridLayout(armors.size(), 1));
		for(int i = 0; i < armors.size(); i++) {
			checkboxes[i] = new JCheckBox();
			checkboxPanel.add(checkboxes[i]);
		}
		for(int i = 0; i < armors.size(); i++) {
			JSONObject curr =((JSONObject) armors.get(i));
			weaponsPanel.add(new JLabel((String) curr.get("name")));
		}
		
		JButton removeWeaponbutton = new JButton("remove Armors from game");
		removeWeaponbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int removed = 0;
				int size = armors.size();
				for(int i = 0; i < size; i++) {
					if(checkboxes[i].isSelected()) {
						armors.remove(i - removed);
						removed++;
					}
				}
				try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getArmorFile())) {
					file.write(armors.toJSONString());
			        file.flush();
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        }
				clearScreen();
				genBaseScreen();
			}
			
		});
		frame.getContentPane().add(checkboxPanel, BorderLayout.WEST);
		frame.getContentPane().add(weaponsPanel, BorderLayout.CENTER);
		frame.getContentPane().add(removeWeaponbutton, BorderLayout.SOUTH);
		frame.pack(); 

		frame.repaint();
	}

	private static JSONArray getAllArmors() throws IOException, ParseException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getArmorFile());
		return (JSONArray) parser.parse(reader);
	}

	protected static void addArmorScreen() {
		JPanel labelPanel = new JPanel(new GridLayout(5, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(5, 1));
		JLabel namelabel = new JLabel("name", JLabel.RIGHT);
		JLabel weightlabel = new JLabel("Weight", JLabel.RIGHT);
		JLabel isHeavyLabel = new JLabel("Is heavy", JLabel.RIGHT);
		JLabel baseACLabel = new JLabel("Base AC", JLabel.RIGHT);
		labelPanel.add(backButton);
		labelPanel.add(namelabel);
		labelPanel.add(weightlabel);
		labelPanel.add(isHeavyLabel);
		labelPanel.add(baseACLabel);
		final JTextField nameInput = new JTextField(20);
		final JTextField weightInput = new JTextField("0");
		final JCheckBox isheavyinput = new JCheckBox();
		final JTextField baseInput = new JTextField("10");
		fieldPanel.add(new JLabel());
		fieldPanel.add(nameInput);
		fieldPanel.add(weightInput);
		fieldPanel.add(isheavyinput);
		fieldPanel.add(baseInput);
		JButton addArmorbutton = new JButton("Add Armor to game");
		final Color defaultBackGround = weightInput.getBackground();
		final JLabel toplabel = new JLabel("Verifying input", JLabel.CENTER);
		addArmorbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				toplabel.setText("Verifying input");
				toplabel.setBackground(Color.GREEN);
				toplabel.setOpaque(true);
				frame.getContentPane().add(toplabel, BorderLayout.NORTH);
				frame.pack(); 
				frame.repaint();
				weightInput.setBackground(defaultBackGround);
				nameInput.setBackground(defaultBackGround);
				baseInput.setBackground(defaultBackGround);
				if(!verifyWeaponInput()) {
					toplabel.setText("Please fix below Errors");
					toplabel.setBackground(Color.RED);
					frame.pack(); 
					frame.repaint();
				}else {
					try {
						new AddNewObject().addArmor(nameInput, weightInput, isheavyinput, baseInput);
						clearScreen();
						genBaseScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
			}

			private boolean verifyWeaponInput() {
				boolean val = true;
				if("".equals(nameInput.getText())) {
					nameInput.setBackground(Color.RED);
					val = false;
				}
				try {
					Integer.parseInt(weightInput.getText());
				}catch(Exception e) {
					weightInput.setBackground(Color.RED);
					val = false;
				}
				try {
					Integer.parseInt(baseInput.getText());
				}catch(Exception e) {
					baseInput.setBackground(Color.RED);
					val = false;
				}
				return val;
			}
		});
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addArmorbutton, BorderLayout.SOUTH);
		frame.pack();
		frame.repaint();
	}

	protected static void removeWeaponScreen() throws IOException, ParseException {
		final JSONArray weapons = getAllWeapons();
		final JCheckBox[] checkboxes = new JCheckBox[weapons.size()];
		JPanel checkboxPanel = new JPanel(new GridLayout(weapons.size(), 1));
		JPanel weaponsPanel = new JPanel(new GridLayout(weapons.size(), 1));
		for(int i = 0; i < weapons.size(); i++) {
			checkboxes[i] = new JCheckBox();
			checkboxPanel.add(checkboxes[i]);
		}
		for(int i = 0; i < weapons.size(); i++) {
			JSONObject curr =((JSONObject) weapons.get(i));
			weaponsPanel.add(new JLabel((String) curr.get("name")));
		}
		
		JButton removeWeaponbutton = new JButton("remove Weapon from game");
		removeWeaponbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int size = weapons.size();
				int removed = 0;
				for(int i = 0; i < size; i++) {
					if(checkboxes[i].isSelected()) {
						weapons.remove(i - removed);
						removed++;
					}
				}
				try (FileWriter file = new FileWriter(config.getResourceFolder() + config.getWeaponsFile())) {
					file.write(weapons.toJSONString());
			        file.flush();
			        } catch (IOException e1) {
			            e1.printStackTrace();
			        }
				clearScreen();
				genBaseScreen();
			}	
		});
		frame.getContentPane().add(checkboxPanel, BorderLayout.WEST);
		frame.getContentPane().add(weaponsPanel, BorderLayout.CENTER);
		frame.getContentPane().add(removeWeaponbutton, BorderLayout.SOUTH);
		frame.pack(); 

		frame.repaint();
	}

	private static JSONArray getAllWeapons() throws IOException, ParseException {
		Reader reader = new FileReader(config.getResourceFolder() + config.getWeaponsFile());
		return (JSONArray) parser.parse(reader);
	}

	public static void clearScreen() {
		frame.getContentPane().removeAll();
	}

	private static void addWeaponScreen() { 	
		JPanel labelPanel = new JPanel(new GridLayout(8, 1));
		JPanel fieldPanel = new JPanel(new GridLayout(8, 1));
		final JTextField nameInput = new JTextField(20);
		final JTextField weightInput = new JTextField("0");
		Integer[] handsInputArray = {1, 2};
		String[] damageDieInputArray = {"D4", "D6", "D8", "D10", "D12"};
		String[] statInputArray = {"str", "dex"};
		final JComboBox<Integer> handsInput = new JComboBox<Integer>(handsInputArray);
		final JComboBox<String> damageDieInput = new JComboBox<String>(damageDieInputArray);
		final JCheckBox getStatOnDam = new JCheckBox(null, null, true);
		final JComboBox<String> statInput = new JComboBox<String>(statInputArray);
		final JCheckBox isheavyinput = new JCheckBox();
		fieldPanel.add(new JLabel());
		fieldPanel.add(nameInput);
		fieldPanel.add(handsInput);
		fieldPanel.add(damageDieInput);
		fieldPanel.add(getStatOnDam);
		fieldPanel.add(statInput);
		fieldPanel.add(weightInput);
		fieldPanel.add(isheavyinput);
		JLabel namelabel = new JLabel("name", JLabel.RIGHT);
		JLabel numHandslabel = new JLabel("Hands", JLabel.RIGHT);
		JLabel damDielabel = new JLabel("Damage die", JLabel.RIGHT);
		JLabel getStatOnDamagelabel = new JLabel("Damge includes stat", JLabel.RIGHT);
		JLabel statlabel = new JLabel("Stat to wield", JLabel.RIGHT);
		JLabel weightlabel = new JLabel("Weight", JLabel.RIGHT);
		JLabel isHeavyLabel = new JLabel("Is heavy", JLabel.RIGHT);
		labelPanel.add(backButton);
		labelPanel.add(namelabel);
		labelPanel.add(numHandslabel);
		labelPanel.add(damDielabel);
		labelPanel.add(getStatOnDamagelabel);
		labelPanel.add(statlabel);
		labelPanel.add(weightlabel);
		labelPanel.add(isHeavyLabel);
		final JLabel toplabel = new JLabel("Verifying input", JLabel.CENTER);
		JButton addWeaponbutton = new JButton("Add Weapon to game");
		final Color defaultBackGround = weightInput.getBackground();;
		addWeaponbutton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				toplabel.setText("Verifying input");
				toplabel.setBackground(Color.GREEN);
				toplabel.setOpaque(true);
				frame.getContentPane().add(toplabel, BorderLayout.NORTH);
				frame.pack(); 
				frame.repaint();
				weightInput.setBackground(defaultBackGround);
				nameInput.setBackground(defaultBackGround);
				if(!verifyWeaponInput(weightInput, nameInput)) {
					toplabel.setText("Please fix below Errors");
					toplabel.setBackground(Color.RED);
					frame.pack(); 
					frame.repaint();
				}else {
					toplabel.setText("Adding weapon " + nameInput.getText() + " to weapons.json");
					frame.pack(); 
					frame.repaint();
					try {
						new AddNewObject().addWeapon(nameInput, weightInput, handsInput, 
								damageDieInput, getStatOnDam, statInput, isheavyinput);
						clearScreen();
						genBaseScreen();
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (ParseException e1) {
						
						e1.printStackTrace();
					}
				}
			}

			private boolean verifyWeaponInput(JTextField weightInput, JTextField nameInput) {
				boolean val = true;
				if("".equals(nameInput.getText())) {
					nameInput.setBackground(Color.RED);
					val = false;
				}
				try {
					Integer.parseInt(weightInput.getText());
				}catch(Exception e) {
					weightInput.setBackground(Color.RED);
					val = false;
				}
				return val;
			}
        });
		frame.getContentPane().add(labelPanel, BorderLayout.WEST);
		frame.getContentPane().add(fieldPanel, BorderLayout.CENTER);
		frame.getContentPane().add(addWeaponbutton, BorderLayout.SOUTH);
		frame.pack(); 
		frame.repaint();
	}
	
	

}

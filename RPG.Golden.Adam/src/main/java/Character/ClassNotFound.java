package Character;

public class ClassNotFound extends Exception {

	private static final long serialVersionUID = 1L;

	public ClassNotFound(String msg) {
		super(msg);
	}

}

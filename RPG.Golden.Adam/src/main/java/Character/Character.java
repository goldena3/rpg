package Character;

import java.util.ArrayList;

import Items.Armor;
import Items.Item;
import Items.Shield;
import Items.Weapon;
import util.my.DiceRoller;
import util.my.Util;

public abstract class Character {
	protected Util myUtil = Util.getInstance();
	protected String Name;
	protected int maxHP = 10;
	protected int currHp = 10;
	protected int type;
	protected int str = 10;
	protected int dex = 10;
	protected int con = 10;
	protected int wis = 10;
	protected int intel = 10;
	protected int cha = 10;
	protected  ArrayList<Item> inventory = new ArrayList<Item>();
	protected int armourClass = 10;
	protected Weapon equippedWeapon;
	protected Armor equipedArmor;
	protected Shield equipedShield;
	protected String className;
	protected boolean[] hands = {false, false};
	protected int inititive;
	
	public int getInititive() {
		return inititive;
	}

	abstract public int equipWeapon(Weapon newWeapon);
	
	abstract public int equipArmour(Armor newArmour);
	
	abstract public int equipShield(Shield newShield);


	public boolean addItem(Item item) {
		if(item == null) {
			return true;
		}
		this.inventory.add(item);
		return true;
	}
	
	public int rollAttack() {
		return util.my.DiceRoller.getInstance().rollDice(1, 20) + this.calcMod("str");
	}
	
	public int getArmourClass() {
		return armourClass;
	}
	
	public boolean usingShield() {
		return equipedShield != null;
	}

	public Character(String n) {
		Name = n;
	}
	
	public Character rollInititive() {
		this.inititive = DiceRoller.getInstance().rollDice(1, 20) + calcMod("dex");
		return this;
	}
	
	public String readableInventory(){
		String inventory = "Armour: " + equipedArmor.getName() + "\n";
		if(usingShield()) {
			inventory += "Shield: " + equipedShield.getName() + "\n";
		}
		inventory += "Weapon: " + equippedWeapon.getName() + "\n\n";
		inventory += "In Inventory:" + "\n";
		inventory += "Name  |  Ammount\n"; 
		Item[] founditems = new Item[this.inventory.size()];
		for(int i = 0; i < this.inventory.size(); i++) {
			if(Util.getInstance().searchItemArrayByName(founditems, this.inventory.get(i).getName()) == -1)
				inventory += this.inventory.get(i).getName() + " | " + Util.getInstance().countNumberOfiteminArrayWithname(this.inventory.toArray(new Item[0]), this.inventory.get(i).getName()) +"\n";
				founditems[i] = this.inventory.get(i);
		}
		inventory += "Total Weight: " + calcTotalweight() + "\n";
		return inventory;
	}
	
	private int calcTotalweight() {
		int totalWeight = 0;
		totalWeight += equippedWeapon.getWeight();
		totalWeight += equipedArmor.getWeight();
		if(usingShield()) {
			totalWeight += equipedShield.getWeight();
		}
		for(int i = 0; i < this.inventory.size(); i++) {
			totalWeight += this.inventory.get(i).getWeight();
		}
		
		return totalWeight;
	}

	public void calcAC() {
		int ac = equipedArmor != null ? equipedArmor.getAcbase() : 10;
		if(equipedShield != null) {
			ac += 2;
		}
		if(equipedArmor == null || !equipedArmor.isHeavy()) {
			ac += calcMod("dex");
		}
		this.armourClass = ac;
	}
	
	public void increseCurrHp(int adjust) {
		currHp += adjust;
		if(currHp > maxHP) {
			currHp = maxHP;
		}
	}
	public void decreaseCurrHp(int adjust) {
		currHp -= adjust;
	}
	
	public int calcMod(String stat) {
		int mod = 0;
		switch(stat) {
			case "str":
				mod = myUtil.calcMod(str);
				break;
			case "dex":
				mod = myUtil.calcMod(dex);
				break;
			case "con":
				mod = myUtil.calcMod(con);
				break;
			case "wis":
				mod = myUtil.calcMod(wis);
				break;
			case "intel":
			case "int":
				mod = myUtil.calcMod(intel);
				break;
			case "cha":
				mod = myUtil.calcMod(cha);
				break;
		}
		return mod;
	}
	
	public int getStr() {
		return str;
	}

	public void setStr(int str) {
		this.str = str;
	}

	public int getDex() {
		return dex;
	}

	public void setDex(int dex) {
		this.dex = dex;
	}

	public int getCon() {
		return con;
	}

	public void setCon(int con) {
		this.con = con;
	}

	public int getWis() {
		return wis;
	}

	public void setWis(int wis) {
		this.wis = wis;
	}

	public int getIntel() {
		return intel;
	}

	public void setIntel(int intel) {
		this.intel = intel;
	}

	public int getCha() {
		return cha;
	}

	public void setCha(int cha) {
		this.cha = cha;
	}
	public int getType() {
		return type;
	}
	protected void setType(int t) {
		this.type = t;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public int getMaxHP() {
		return maxHP;
	}
	public void setMaxHP(int maxHP) {
		this.maxHP = maxHP;
		this.currHp = maxHP;
	}
	public int getCurrHp() {
		return currHp;
	}
	
	
	public int rollDamage(boolean crit) {
		DiceRoller roller = DiceRoller.getInstance();
		int damageDone = (equippedWeapon.isGetBonusOnDam() ? (equippedWeapon.getDamStat().equals("str") ? calcMod("str") : calcMod("dex")) : 0);
		damageDone += roller.rollDice((crit ? 2 : 1), equippedWeapon.getDamageDie());
		return damageDone;
	}
	
	public int rollDamage() {
		return rollDamage(false);
	}

	public boolean isDead() {
		return this.currHp <= 0;
	}
	
}

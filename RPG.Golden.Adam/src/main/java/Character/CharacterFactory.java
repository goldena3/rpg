package Character;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import Items.Armor;
import Items.ArmorNotFoundException;
import Items.FoodNotFoundException;
import Items.ItemFactory;
import Items.ItemNotFoundException;
import Items.PotionNotFoundException;
import Items.Shield;
import Items.UnknowItemTypeException;
import Items.Weapon;
import Items.WeaponNotFounException;
import util.my.Config;

public class CharacterFactory {
	private static JSONParser parser = new JSONParser();
	private  Config config = Config.getInstance();
	private final int PLAYER = 1;
	private final int NPC = 2;
	private final int ENEMEY= 3;
	private static ItemFactory  itemFactory = ItemFactory.getInstance();

	private static CharacterFactory instance;
	
	private CharacterFactory() {}
	
	public static CharacterFactory getInstance() {
		if(instance == null) {
			instance = new CharacterFactory();
		}
		return instance;
	}
	
	public Npc generateNpc(JSONObject npc) throws WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, IOException, ParseException, PotionNotFoundException {
		Npc newPeep = new Npc((String) npc.get("name"));
		newPeep.setType(NPC);
		newPeep = (Npc) setEquepment (newPeep, npc);
		if(npc.containsKey("description")) {
			newPeep.setDescription((String) npc.get("description"));
		}
		newPeep.calcAC();
		return newPeep;
	}
	
	public int getPLAYER() {
		return PLAYER;
	}

	public int getNPC() {
		return NPC;
	}

	public int getENEMEY() {
		return ENEMEY;
	}

	public Enemy generateEnemy(JSONObject enemy) throws WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, IOException, ParseException, PotionNotFoundException {
		Enemy peep = new Enemy((String) enemy.get("name"));
		peep.setType(this.ENEMEY);
		peep = (Enemy) this.setEquepment(peep, (JSONObject) enemy.get("gear"));
		peep = (Enemy) this.setStats(peep, (JSONObject) enemy.get("stats"));
		peep.setMaxHP((int) ((long) enemy.get("hit_points")));
		peep.calcAC();
		return peep;
	}
	
	private Character setStats(Character peep, JSONObject stats) {
		final String[] statOrder = {"str", "dex", "con", "int", "wis", "cha"};
		for(int i = 0; i < statOrder.length; i++) {
			switch(statOrder[i]) {
			case "str":
				peep.setStr((int) ((long) stats.get(statOrder[i])));
				break;
			case "dex":
				peep.setDex((int) ((long) stats.get(statOrder[i])));
				break;
			case "con":
				peep.setCon((int) ((long) stats.get(statOrder[i])));
				break;
			case "intel":
			case "int":
				peep.setWis((int) ((long) stats.get(statOrder[i])));
				break;
			case "wis":
				peep.setIntel((int) ((long) stats.get(statOrder[i])));
				break;
			case "cha":
				peep.setCha((int) ((long) stats.get(statOrder[i])));
				break;
		}
		}
		return peep;
	}

	public Player generatePlayer(String name, String className) throws ClassNotFound, WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, FileNotFoundException, IOException, ParseException, PotionNotFoundException {
		Player newPeep = new Player(name);
		newPeep.setType(this.PLAYER);
		 Reader reader = new FileReader(config.getResourceFolder() + config.getClassesFile()); 
		 JSONArray classes = (JSONArray) parser.parse(reader);
		 newPeep = setUpClassStart(newPeep, className, classes);
		return newPeep;
	}
	
	private Player setUpClassStart(Player newPeep, String className, JSONArray classes) throws ClassNotFound, WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, IOException, ParseException, PotionNotFoundException {
		Iterator<JSONObject> itr = classes.iterator();
		JSONObject foundClass = null;
		while(itr.hasNext() & foundClass == null) {
			JSONObject curr = itr.next();
			if(curr.get("name").equals(className)) {
				foundClass = curr;
			}
		}
		if(foundClass == null) {
			throw new ClassNotFound("Unable to find class " + className + ". In " + config.getClassesFile());
		}
		newPeep.className = (String) foundClass.get("name");
		newPeep.setHitDie((long) foundClass.get("hit_die"));
		newPeep.setInitialHP();
		newPeep = (Player) setEquepment(newPeep, (JSONObject) foundClass.get("starting_gear"));
		newPeep.setType(PLAYER);
		
		return newPeep;
	}

	private Character setEquepment(Character newPeep, JSONObject startingGear) throws WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, IOException, ParseException, PotionNotFoundException {
		JSONArray equipped = (JSONArray) startingGear.get("equipped");
		JSONArray inventory = (JSONArray) startingGear.get("in_inventory");
		Iterator<JSONObject> itr = equipped.iterator();
		JSONObject curr;
		while(itr.hasNext()) {
			curr = itr.next();
			switch(((String) curr.get("type")).toLowerCase()) {
				case "weapon":
					newPeep.equipWeapon((Weapon) itemFactory.genItem(curr));
					break;
				case "shield":
					newPeep.equipShield((Shield) itemFactory.genItem(curr));
					break;
				case "armor":
				case "armour":
					newPeep.equipedArmor = (Armor) itemFactory.genItem(curr);
					break;
				default:
					throw new UnknowItemTypeException("Unknow type " + (String) curr.get("type"));
			}
		}
		itr = inventory.iterator();
		while(itr.hasNext()) {
			curr = itr.next();
			long amount = 1;
			if(curr.containsKey("amount")) {
				amount = (long) curr.get("amount");
			}
			for(int i = 0; i < amount; i++) {
				newPeep.addItem(itemFactory.genItem(curr));
			}
		}
		return newPeep;
	}
	
}

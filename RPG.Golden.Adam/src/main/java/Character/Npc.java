package Character;

import Items.Armor;
import Items.Shield;
import Items.Weapon;

public class Npc extends Character {
	private boolean isDead = false;
	private String description;
	
	public Npc(String n) {
		super(n);
	}

	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public int equipWeapon(Weapon newWeapon) {
		if(equipedShield != null && newWeapon.getNumHands() == 2) {
			return myUtil.NOTENOUGHHANDS;
		}
		Weapon curr = null;
		if(equippedWeapon != null) {
			curr = equippedWeapon;
			switch((int) curr.getNumHands()) {
				case 2:
					hands[1] = false;
				case 1:
					hands[0] = false;
			}
		}
		
		equippedWeapon = newWeapon;
		switch((int) newWeapon.getNumHands()) {
			case 2:
				hands[1] = true;
			case 1:
				hands[0] = true;
		}
		return myUtil.SUCCESS;
	}
	
	public int equipArmour(Armor newArmour) {
		if(this.equipedArmor != null) {
			Armor curr = equipedArmor;
		}
		equipedArmor = newArmour;
		this.calcAC();
		return myUtil.SUCCESS;
	}
	
	public int equipShield(Shield newShield) {
		if(equippedWeapon != null) {
			if(equippedWeapon.getNumHands() == 2) {
				return myUtil.NOTENOUGHHANDS;
			}
		}
		Shield curr = null;
		if(equipedShield != null) {
			curr = equipedShield;
		}
		equipedShield = newShield;
		hands[1] = true;
		this.calcAC();
		return myUtil.SUCCESS;
	}

}

package Character;

import java.util.Arrays;

import Items.Armor;
import Items.Shield;
import Items.Weapon;

public class Enemy extends Character {

	public Enemy(String n) {
		super(n);
		}

	@Override
	public int equipWeapon(Weapon newWeapon) {
		this.equippedWeapon = newWeapon;
		return myUtil.SUCCESS;
	}

	@Override
	public int equipArmour(Armor newArmour) {
		this.equipedArmor = newArmour;
		return myUtil.SUCCESS;
	}

	@Override
	public int equipShield(Shield newShield) {
		this.equipedShield = newShield;
		return myUtil.SUCCESS;
	}

	@Override
	public String toString() {
		return "Enemy [myUtil=" + myUtil + ", Name=" + Name + ", maxHP=" + maxHP + ", currHp=" + currHp + ", type="
				+ type + ", str=" + str + ", dex=" + dex + ", con=" + con + ", wis=" + wis + ", intel=" + intel
				+ ", cha=" + cha + ", inventory=" + inventory + ", armourClass=" + armourClass + ", equippedWeapon="
				+ equippedWeapon + ", equipedArmor=" + equipedArmor + ", equipedShield=" + equipedShield
				+ ", className=" + className + ", hands=" + Arrays.toString(hands) + "]";
	}

}

package Character;

import java.util.ArrayList;
import java.util.Arrays;

import Items.*;
import RPG.Journal;
import util.my.DiceRoller;

public class Player extends Character {
	private boolean fullInventory = false;
	private boolean[] hands = {false, false};
	private long hitDie;
	private int level = 1;
	private Journal journal = new Journal();
	
	public Player(String n) {
		super(n);
	}
	
	public ArrayList<Item> getInventory() {
		return inventory;
	}

	public void removeItemByname(String name) {
		int index = myUtil.searchItemArrayByName((Item[]) inventory.toArray(), name);
		if(index == -1) {
			return;
		}
		this.inventory.remove(index);
	}
	
	public void removeItemByIndex(int index) {
		this.inventory.remove(index);
	}
	
	
	public boolean[] getHands() {
		return hands;
	}

	public void displayInventory() {
		for(int i = 0; i < inventory.size(); i++) {
			if(inventory.get(i) != null) {
				myUtil.println((i+1) + ": " + inventory.get(i).getName());
			}
		}
	}
	
	public int equipWeapon(Weapon newWeapon) {
		if(fullInventory) {
			return myUtil.NOTENOUGHSPACE;
		}
		if(equipedShield != null && newWeapon.getNumHands() == 2) {
			return myUtil.NOTENOUGHHANDS;
		}
		Weapon curr = null;
		if(equippedWeapon != null) {
			curr = equippedWeapon;
			switch((int) curr.getNumHands()) {
				case 2:
					hands[1] = false;
				case 1:
					hands[0] = false;
			}
		}
		
		equippedWeapon = newWeapon;
		switch((int) newWeapon.getNumHands()) {
			case 2:
				hands[1] = true;
			case 1:
				hands[0] = true;
		}
		this.addItem(curr);
		return myUtil.SUCCESS;
	}
	
	public int equipArmour(Armor newArmour) {
		if(fullInventory && this.equipedArmor != null) {
			return myUtil.NOTENOUGHSPACE;
		}
		if(this.equipedArmor != null) {
			Armor curr = equipedArmor;
			this.addItem(curr);		
		}
		equipedArmor = newArmour;
		this.calcAC();
		return myUtil.SUCCESS;
	}
	
	public int equipShield(Shield newShield) {
		if(fullInventory && equipedShield != null) {
			return myUtil.NOTENOUGHSPACE;
		}
		if(equippedWeapon != null) {
			if(equippedWeapon.getNumHands() == 2) {
				return myUtil.NOTENOUGHHANDS;
			}
		}
		Shield curr = null;
		if(equipedShield != null) {
			curr = equipedShield;
		}
		equipedShield = newShield;
		hands[1] = true;
		this.addItem(curr);
		this.calcAC();
		return myUtil.SUCCESS;
	}
	
	public int unEquipShield() {
		if(fullInventory && equipedShield != null) {
			return myUtil.NOTENOUGHSPACE;
		}else if(equipedShield != null) {
			this.addItem(equipedShield);
			equipedShield = null;
			hands[1] = false;
		}
		return myUtil.SUCCESS;
	}

	public long getHitDie() {
		return hitDie;
	}

	public void setHitDie(long l) {
		this.hitDie = l;
	}
	
	public void increaseMaxHPByHitDie() {
		int increase = DiceRoller.getInstance().rollDice(1, hitDie) + this.calcMod("con");
		this.maxHP +=  increase;
		increseCurrHp(increase);
	}

	public void setInitialHP() {
		this.maxHP = (int) hitDie + this.calcMod("con");
		this.currHp = (int) hitDie;
	}
	
	private void increaseMaxHpAfterLevelUp() { 
		int increaseAmount =  DiceRoller.getInstance().rollDice(1, this.hitDie) + this.calcMod("con");
		this.maxHP += increaseAmount;
		this.currHp += increaseAmount;
	}

	@Override
	public String toString() {
		return "Player [fullInventory=" + fullInventory + ", hands=" + Arrays.toString(hands) + ", hitDie=" + hitDie
				+ ", myUtil=" + myUtil + ", Name=" + Name + ", maxHP=" + maxHP + ", currHp=" + currHp + ", type=" + type
				+ ", str=" + str + ", dex=" + dex + ", con=" + con + ", wis=" + wis + ", intel=" + intel + ", cha="
				+ cha + ", inventory=" + Arrays.deepToString((inventory.toArray())) + ", armourClass=" + armourClass
				+ ", equippedWeapon=" + equippedWeapon + ", equipedArmor=" + equipedArmor + ", equipedShield="
				+ equipedShield + ", className=" + className + "]";
	}

	public int getLevel() {
		return level;
	}

	public void levelUp() {
		this.level++;
		this.increaseMaxHpAfterLevelUp();
	}
	
	public void addToJournal(String entry) {
		this.journal.addToJournal(entry);
	}
	
	public String viewJournal() {
		return this.journal.viewJournal();
	}

	public String getHumanReadableCombatStats() {
		StringBuilder builder = new StringBuilder();
		builder.append("HP: " + this.currHp + "\n");
		
		return builder.toString();
	}

}

package RPG;
import java.io.IOException;
import java.util.UUID;

import org.json.simple.parser.ParseException;

import Character.Player;
import Map.Location;
import Map.Map;

public class AdditionAction implements EventConection {
	private String name;
	private UUID id = UUID.randomUUID();
	private boolean locationIsRoom;
	private boolean active = true;
	private boolean oneTimeAction;
	private Location location;
	private String baseText;
	private boolean isRepeatable;
	private boolean justRan = false;
	
	public AdditionAction(String n, String LocationName, boolean locationIsRoom, boolean oneTimeAction, String baseText, boolean isRepeatable) throws Exception {
		this.name = n;
		this.locationIsRoom = locationIsRoom;
		this.setLocation(Map.getInstance().FindaLocation(LocationName, this.locationIsRoom ? "room": "city"));
		this.oneTimeAction = oneTimeAction;
		this.baseText = baseText;
		this.isRepeatable = isRepeatable;
	}
	
	public AdditionAction(String n, String LocationName, boolean locationIsRoom, boolean oneTimeAction, String baseText, boolean isRepeatable, boolean isActive) throws Exception {
		this.name = n;
		this.locationIsRoom = locationIsRoom;
		this.setLocation(Map.getInstance().FindaLocation(LocationName, this.locationIsRoom ? "room": "city"));
		this.oneTimeAction = oneTimeAction;
		this.active = isActive;
		this.baseText = baseText;
	}
	
	public String getName() {
		return name;
	}

	public UUID getId() {
		return id;
	}

	public boolean isActive() {
		return active;
	}
	
	@Override
	public String toString() {
		return "AdditionAction [name=" + name + ", id=" + id + ", locationIsRoom=" + locationIsRoom + ", active="
				+ active + ", oneTimeAction=" + oneTimeAction + ", location=" + location + ", baseText=" + baseText
				+ ", isRepeatable=" + isRepeatable + ", justRan=" + justRan + "]";
	}

	public void runAction() throws IOException, ParseException {
		if(oneTimeAction) {
			active = false;
		}
		justRan = true;
		this.addItemToJournal();
	}

	public Location getLocation() {
		return location;
	}

	private void setLocation(Location location) {
		this.location = location;
	}
	
	public Boolean equals(AdditionAction action) {
		return this.id.equals(action.getId());
	}
	
	public String getBaseText() {
		return this.baseText;
	}

	public boolean isRepeatable() {
		return isRepeatable;
	}

	public boolean isJustRan() {
		return justRan;
	}
	
	public void resetAction() {
		this.justRan = false;
	}

	@Override
	public void addItemToJournal() throws IOException, ParseException {
		((Player) RunRpg.getinstance().getPlayer()).addToJournal(baseText);
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}
}

package RPG;

import java.io.IOException;

import org.json.simple.parser.ParseException;

import Map.Location;
import Map.LocationNotFoundException;
import Map.Map;
import Map.Route;

public class ConnectionStatus {
	EventConection connection;
	private String connectionType;
	private String eventName;
	
	public ConnectionStatus(String connectionType, EventConection connection ) {
		this.connectionType = connectionType;
		this.connection = connection;
	}
	public ConnectionStatus(String connectionType, String eventName) {
		this.connectionType = connectionType;
		this.eventName = eventName;
	}
	
	public String getConnectionType() {
		return connectionType;
	}
	
	@Override
	public String toString() {
		return "ConnectionStatus [connection=" + connection + ", connectionType=" + connectionType + ", eventName="
				+ eventName + "]";
	}
	public boolean checkStatus() throws IOException, ParseException, LocationNotFoundException {
		switch(connectionType.toLowerCase()) {
			case "action":
				return ((AdditionAction)connection).isJustRan();
			case "route":
				return ((Route) connection).isJustTraveled();	
			case "player location":
				return Map.getInstance().playerAtLocation(((Location)connection));
			case "event":
				return RunRpg.getinstance().eventHasRun(eventName);
			default:
				return false;
			}
	}
	public String getEventName() {
		return eventName;
	}
}

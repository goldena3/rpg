package RPG;

import java.io.IOException;

import org.json.simple.parser.ParseException;

import Character.Player;

public abstract class Event implements EventConection{
	protected String name;
	private String type;
	protected boolean repeatable = true;
	protected boolean hasRun = false;
	
	public Event(String name, String type) {
		this.name = name;
		this.type = type;
	}
	public Event(String name, String type, boolean isRepetable) {
		this.name = name;
		this.type = type;
		repeatable = isRepetable;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}
	
	public void runEvent() throws IOException, ParseException {
		hasRun = true;
		System.out.println("running event: " + name);
		System.out.println(this.toString());
	}

	
	public boolean getHasRun() {
		return this.hasRun;
	}

	@Override
	public String toString() {
		return  this.getClass().getName() + " [name=" + name + ", type=" + type + ", repeatable=" + repeatable + ", hasRun=" + hasRun + "]";
	}

	public boolean isRepeatable() {
		return repeatable;
	}
	
	@Override
	public void addItemToJournal() throws IOException, ParseException {
		((Player) RunRpg.getinstance().getPlayer()).addToJournal(this.name);
	}
}

package RPG;

import java.io.IOException;

import org.json.simple.parser.ParseException;

public interface EventConection {
	
	public String journalItem = "";

	public String name = "";
	
	public void addItemToJournal() throws IOException, ParseException;
	
	public  void setName(String name);
	
	public default String getName() {
		return name;
	}
	

}

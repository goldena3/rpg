package RPG;

import java.io.IOException;

import org.json.simple.parser.ParseException;

import util.my.DialogTree;

public class Dialog extends Event {
	
	private DialogTree dialog;

	public Dialog(String name, String type, boolean isRepetable) throws IOException, ParseException {
		super(name, type, isRepetable);
		buildTree();
	}

	public Dialog(String name, String type) throws IOException, ParseException {
		super(name, type);
		buildTree();
	}
	
	private void buildTree() throws IOException, ParseException {
		dialog = new DialogTree(name);
	}
	
	@Override
	public void runEvent() throws IOException, ParseException {
		dialog.runDialog();
		this.addItemToJournal();
		this.hasRun = true;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}

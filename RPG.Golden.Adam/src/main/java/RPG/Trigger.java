package RPG;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import Map.LocationNotFoundException;
import Map.Map;

public class Trigger {
	private Event event;
	List<ConnectionStatus> stauses = new ArrayList<ConnectionStatus>();
	private boolean justTriggered = false;
	
	public Trigger(JSONObject event, JSONArray connections) throws Exception {
		boolean isRepeatable = event.containsKey("isRepetable") ? (boolean) event.get("isRepetable") : false;
		switch((String) event.get("type")) {
			case "combat":
				this.event = new Combat((String) event.get("name"), (String) event.get("type"), isRepeatable);
				break;
			case "dialoge":
				this.event = new Dialog((String) event.get("name"), (String) event.get("type"), isRepeatable);
				break;
		}
		
		
		for(int i = 0; i < connections.size(); i++) {
			JSONObject currConnection = (JSONObject) connections.get(i);
			EventConection currEventConection = null;
			switch((String) currConnection.get("type")) {
				case "action":
					currEventConection = RunRpg.getinstance().findAction((String) currConnection.get("name"));
					break;
				case "event":
					stauses.add(new ConnectionStatus("event", (String) currConnection.get("name")));
					break;
				case "route":
					currEventConection = Map.getInstance().findRoute((JSONArray) currConnection.get("points"));
					break;
				case "player location":
					String[] locationTypes = {"city", "building", "room"};
					for(int j = 0; j < locationTypes.length && currEventConection == null; j++) {
						currEventConection = Map.getInstance().FindaLocation((String) currConnection.get("location"), locationTypes[j]);
					}
					break;
			}
			if(currEventConection != null) {
				stauses.add(new ConnectionStatus((String) currConnection.get("type"), currEventConection));
			}
		}
	}
	
	@Override
	public String toString() {
		return "Trigger [event=" + event + ", stauses=" + stauses + "]";
	}

	public Event getEvent() {
		return event;
	}
	
	public boolean checkStatus() throws IOException, ParseException, LocationNotFoundException {
		if(justTriggered || (event.getHasRun() && !event.isRepeatable())) {
			return false; 
		}
		
		for(ConnectionStatus status: stauses) {
			System.out.println("checking status " + status.getConnectionType());
			if(!status.checkStatus()) {
				return false;
			}
		}
		
		return true;
		
	}

	public void resetTrigger() {
		this.justTriggered = false;
	}

	
}

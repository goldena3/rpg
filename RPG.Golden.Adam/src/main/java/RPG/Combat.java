package RPG;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import Character.Character;
import Character.Player;
import Items.ArmorNotFoundException;
import Items.FoodNotFoundException;
import Items.ItemNotFoundException;
import Items.PotionNotFoundException;
import Items.UnknowItemTypeException;
import Items.WeaponNotFounException;
import Character.CharacterFactory;
import Character.Enemy;
import Character.Npc;
import util.my.Util;

@SuppressWarnings({"unchecked", "rawtypes"})
public class Combat extends Event {
	
	private List<Character> enemies = new ArrayList();
	private List<Character> initiative  = new ArrayList<>();
	private static final int ENEMY = 3;
	private static final int NPC = 2;
	private static final int PLAYER = 1;
	private boolean hasNpc = false;
	private static final Util util = Util.getInstance();
	private static final String ATTACK = "Attack";
	private static final String VIEW_STATS = "View Stats";

	public Combat(String name, String type) throws IOException, ParseException, WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, PotionNotFoundException {
		super(name, type);
		buildEncounter();
	}
	
	public Combat(String name, String type, boolean isRepetable) throws IOException, ParseException, WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, PotionNotFoundException {
		super(name, type, isRepetable);
		buildEncounter();
	}
	
	
	private void buildEncounter() throws IOException, ParseException, WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, PotionNotFoundException {
		JSONArray allEvents = util.parseFile("events");
		JSONObject enemies = getEnemies(allEvents);
		Iterator<String> itr = enemies.keySet().iterator();
		while(itr.hasNext()) {
			String name = (String) itr.next();
			JSONObject jsonEnemy = getEmenyFromFile(name);
			long amount =  (long) enemies.get(name);
			for(int i = 0; i < amount; i++) {
				this.enemies.add(CharacterFactory.getInstance().generateEnemy(jsonEnemy));
			}
		}
	}

	private JSONObject getEmenyFromFile(String name) throws IOException, ParseException {
		JSONArray allEnemies = util.parseFile("eniemies");
		for(int i = 0; i < allEnemies.size(); i++) {
			if(((String)((JSONObject)allEnemies.get(i)).get("name")).equals(name)) {
				return (JSONObject)allEnemies.get(i);
			}
		}
		return null;
	}

	private JSONObject getEnemies(JSONArray allEvents) {
		for(int i = 0; i < allEvents.size(); i++) {
			if(((String)((JSONObject)allEvents.get(i)).get("name")).equals(name)) {
				return (JSONObject) ((JSONObject)allEvents.get(i)).get("enemies");
			}
		}
		return null;
	}

	@Override
	public void addItemToJournal() throws IOException, ParseException {
		((Player) RunRpg.getinstance().getPlayer()).addToJournal(this.name);
	}

	@Override
	public String toString() {
		return "Combat [enemies=" + enemies + "]";
	}
	
	@Override
	public void runEvent() throws IOException, ParseException {
		util.println("running event: " + name);
		initiative.addAll(enemies);
		initiative.add(RunRpg.getinstance().getPlayer());
		initiative = initiative.stream().map(Character::rollInititive)
				.collect(Collectors.toList()).stream()
				.sorted(Comparator.comparing(Character::getInititive).reversed())
				.collect(Collectors.toList());
		this.runCombat();
		hasRun = true;
		if(this.isRepeatable()) {
			this.resetCombat();
		}
	}

	private void resetCombat() {
		enemies.stream()
		.forEach(enemy -> enemy.increseCurrHp(enemy.getMaxHP()*5));
	}

	private void runCombat() throws IOException, ParseException {
		while(isComabatover()) {
			runRound();
		}
	}

	private boolean isComabatover() {
		return enemies.stream()
				.anyMatch(enemy -> enemy.getType() == ENEMY && !enemy.isDead());
	}

	private void runRound() throws IOException, ParseException {
		for(Character character: initiative) {
			if(character.getType() == PLAYER){
				runPlayerAction((Player) character);
			}else {
				runAiAction(character);
			}
		}
	}

	private void runPlayerAction(Player player) {
		String[] baseActions = {ATTACK , VIEW_STATS};
		List<String> allActions = new ArrayList();
		allActions.addAll(Arrays.asList(baseActions));
		util.println("What do you want to do?");
		util.printInstructionsArray(allActions);
		
		switch(allActions.get(util.getIndexFromRange(allActions.size()))) {
		case ATTACK:
				attack(player);
			break;
		case VIEW_STATS:
			util.println(player.getHumanReadableCombatStats());
			runPlayerAction(player);
			break;
		default:
			util.println("Unknown Action");
		}
		
		
	}

	private void attack(Player player) {
		List<Character> inRange =  initiative.stream()
				.filter(enemy -> (enemy.getType() == ENEMY && !enemy.isDead()))
				.collect(Collectors.toList());
		util.println("What enemy do you want to attack?");
		util.printInstructionsArray(inRange.stream()
				.map(Character::getName)
				.collect(Collectors.toList()));
		int choice = util.getIndexFromRange(inRange.size());
		int toHit = player.rollAttack();
		boolean doesHit = toHit >= inRange.get(choice).getArmourClass();
		boolean isCrit = 20 == (toHit - util.calcMod(player.getStr()));
		if(isCrit) {
			util.println("You Crit!");
		}else {
			util.println("You rolled a " + toHit + ". You " + (doesHit ? "hit" : "miss"));
		}
		if(doesHit) {
			int damageDealt = player.rollDamage(isCrit);
			util.println("You delt " + damageDealt + " damage");
			inRange.get(choice).decreaseCurrHp(damageDealt);
			if(inRange.get(choice).isDead()) {
				util.println("You killed a " + inRange.get(choice).getName());
			}
		}
	}

	private void runAiAction(Character character) throws IOException, ParseException {
		
		switch(character.getType()) {
			case ENEMY:
				Enemy enemy = (Enemy) character;
				if(enemy.isDead()) {
					return;
				}
				if(!hasNpc) {
					int toHit = enemy.rollAttack();
					if( toHit >= RunRpg.getinstance().getPlayer().getArmourClass()) {
						RunRpg.getinstance().getPlayer()
						.decreaseCurrHp(
						enemy.rollDamage(toHit - util.calcMod(enemy.getStr()) == 20));
						if(RunRpg.getinstance().getPlayer().getCurrHp() < 0) {
							util.println("You died");
							System.exit(0);
						}
					}
				}
				break;
			case NPC:
				Npc npc = (Npc) character;
				break;
			default:
				util.println("Unrecongnized Type " + character.getType());
		}
		
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

}

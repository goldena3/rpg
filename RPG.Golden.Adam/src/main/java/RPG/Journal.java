package RPG;

import java.util.ArrayList;

public class Journal {
	
	private ArrayList<String> journal = new ArrayList<String>();
	
	public void addToJournal(String entry) {
		journal.add(entry);
	}
	
	public String viewJournal() {
		String retVal = "";
		for(int i = 0; i < journal.size(); i++) {
			retVal += journal.get(i) + "\n";
		}
		return retVal;
	}

}

package RPG;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import Character.*;
import Character.Character;
import Items.*;
import Map.LocationNotFoundException;
import Map.Map;
import util.my.*;

@SuppressWarnings("unchecked")
public class RunRpg {
	private JSONObject story;
	private boolean gameOver = false;
	private Util util = Util.getInstance();
	@Override
	public String toString() {
		return "RunRpg [story=" + story + ", player=" + player + ", map=" + map + "]";
	}
	
	private static RunRpg instance = null;
	
	public static RunRpg getinstance() throws IOException, ParseException {
		if(instance == null) {
			instance = new RunRpg(Util.getInstance().parseFileToJsonObject("story"));
		}
		return instance;
	}

	private Character player;
	public Character getPlayer() {
		return player;
	}
	private ArrayList<Npc> npcs = new ArrayList<Npc>();
	private Map map;
	private  Config config = Config.getInstance();
	private static  Util myUtil = Util.getInstance();
	private  JSONParser parser = new JSONParser();
	private CharacterFactory characterFactory = CharacterFactory.getInstance();
	final private String[] statOrder = {"str", "dex", "con", "int", "wis", "cha"};
	private ArrayList<AdditionAction> actions = new ArrayList<AdditionAction>();
	private ArrayList<Trigger> triggers = new ArrayList<Trigger>();
	
	private RunRpg(JSONObject story) {
		this.story = story;
	}
	
	public AdditionAction findAction(String name) {
		for(int i = 0; i < actions.size(); i++) {
			if(actions.get(i).getName().equals(name)) {
				return actions.get(i);
			}
		}
		return null;
	}
	
	public void start() throws Exception {
		Thread builder = new Thread(new generateGameStart());
		builder.start();
		genCharacter();
		while(builder.isAlive()) {}
		this.map = Map.getInstance();
		map.moveToCity((String) ((JSONObject) story.get("setup")).get("start_city"));
		gameLoop();
	}

	private void genEventsAndTriggers() throws Exception {
		JSONArray jsonEvents = util.parseFile("events");
		JSONArray jsonTriggers = util.parseFile("triggers");
		for(int i = 0; i < jsonEvents.size(); i++) {
			JSONObject currEvent = (JSONObject) jsonEvents.get(i);
			for(int j = 0; j < jsonTriggers.size(); j++) {
				if(((String)((JSONObject) jsonTriggers.get(i)).get("event name")).equals((String)(currEvent.get("name")))) {
					this.triggers.add(new Trigger(currEvent, ((JSONArray)((JSONObject) jsonTriggers.get(i)).get("connections"))));
					j = jsonTriggers.size();
				}
			}
		}

	}

	private void genActions() throws Exception {
		JSONArray jsonActions = util.parseFile("actions");
		for(int i = 0; i < jsonActions.size(); i++) {
			JSONObject curr = (JSONObject) jsonActions.get(i);
			if(curr.containsKey("isActive")) {
				actions.add(new AdditionAction((String) curr.get("name"), (String) curr.get("locationName"), (boolean) curr.get("locationIsRoom"), (boolean) curr.get("oneTimeAction"), (String) curr.get("baseText"), (boolean) curr.get("isRepeatable"), (boolean) curr.get("isActive") ));
			}else {
				actions.add(new AdditionAction((String) curr.get("name"), (String) curr.get("locationName"), (boolean) curr.get("locationIsRoom"), (boolean) curr.get("oneTimeAction"),  (String) curr.get("baseText"),(boolean) curr.get("isRepeatable")));
			}
		}
	}

	private void genNpc() throws IOException, ParseException, WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, PotionNotFoundException {
		JSONArray jsonNpc = util.parseFile("npc");
		for(int i = 0; i < jsonNpc.size(); i++) {
			npcs.add(characterFactory.generateNpc((JSONObject) jsonNpc.get(i)));
		}
	}

	private void gameLoop() throws IOException, ParseException, Exception {
		String[] baseCommands = {"View Map", "Travel to city", "View Inventory", "Go to bulding in City", "End Game"};
		ArrayList<String> commands = new ArrayList<String>();
		commands = addArrayToArrayList(commands, baseCommands);
		while(!gameOver) {
			if(map.isPlayerInBuilding() && !commandIsAdded(commands, "Move Rooms")) {
				commands.add("Move Rooms");
			}else if(!map.isPlayerInBuilding()) {
				commands = removeCommand(commands, "Move Rooms");
			}
			ArrayList<AdditionAction> addedCommands = new ArrayList<AdditionAction>();
			for(int i = 0; i < actions.size(); i++) {
				if(!commandIsAdded(commands, actions.get(i).getName()) && actions.get(i).isActive() && actions.get(i).getLocation().equals((map.isPlayerInBuilding() ? 
						map.FindaLocation(map.getPlayerRoomLocation(), "room") :  
						map.FindaLocation(map.getPlayerLocation(), "city")))){
					commands.add(actions.get(i).getName());
					addedCommands.add(actions.get(i));
				}else if((actions.get(i).isJustRan() && !actions.get(i).isRepeatable()) || !actions.get(i).getLocation().equals((map.isPlayerInBuilding() ? 
						map.FindaLocation(map.getPlayerRoomLocation(), "room") :  
						map.FindaLocation(map.getPlayerLocation(), "city")))){
					commands = removeCommand(commands, actions.get(i).getName());
					addedCommands.remove(actions.get(i));
				}
				actions.get(i).resetAction();
			}
			util.println("What would you like to do?");
			util.printInstructionsArray(commands.toArray(new String[0]));
			runCommand(commands.get(util.getIndexFromRange(commands.size())), addedCommands);
			checkTriggers();
			map.resetRoutes();
			util.println(((Player) this.player).viewJournal());
			
		}
	}

	private void checkTriggers() throws IOException, ParseException, LocationNotFoundException {
		for(int i= 0; i < this.triggers.size(); i++) {
			if(triggers.get(i).checkStatus()) {
				switch (triggers.get(i).getEvent().getType()) {
				case "dialoge":
					((Dialog)triggers.get(i).getEvent()).runEvent();
					break;
				case "combat":
					((Combat)triggers.get(i).getEvent()).runEvent();
					break;
				default:
					triggers.get(i).getEvent().runEvent();
				}
				i = 0;
			}
		}
		for(int i= 0; i < this.triggers.size(); i++) {
			triggers.get(i).resetTrigger();
		}
	}

	private ArrayList<String> removeCommand(ArrayList<String> commands, String command) {
		for(int i = 0; i < commands.size(); i++) {
			if(commands.get(i).equals(command)) {
				commands.remove(i);
				return commands;
			}
		}
		return commands;
	}

	private boolean commandIsAdded(ArrayList<String> commands, String command) {
		for(int i = 0; i < commands.size(); i++) {
			if(commands.get(i).equals(command)) {
				return true;
			}
		}
		return false;
	}

	private ArrayList<String> addArrayToArrayList(ArrayList<String> commands, String[] baseCommands) {
		for(int i = 0; i < baseCommands.length; i++) {
			commands.add(baseCommands[i]);
		}
		return commands;
	}

	private void runCommand(String command, ArrayList<AdditionAction> addedCommands) throws LocationNotFoundException, IOException, ParseException {
		switch(command) {
		case "End Game":
			util.println("Are you sure you wish to exit?");
			if(util.verifyInput()) {
				System.exit(0);
			}
			break;
		case "View Map":
			util.println(map.humanReadableMap());
			break;
		case "Travel to city":
			String[] adjacientCities = map.getPlayerAdjacientCities();
			if(adjacientCities.length < 1) {
				util.println("No adjacient cities to travel to");
				return;
			}
			util.println("Which city do you want travel to");
			util.printInstructionsArray(adjacientCities);
			int index = util.getIndexFromRange(adjacientCities.length);
			map.moveToCity(adjacientCities[index]);
			break;
		case "View Inventory":
			util.println(player.readableInventory());
			break;
		case "Go to bulding in City":
			String[] cityBuildngs = map.getCurrentCityBuildings();
			if(cityBuildngs.length < 1) {
				util.println("No Building in the current city");
				return;
			}
			util.println("What building do you want to move to");
			util.printInstructionsArray(cityBuildngs);
			map.moveTobuilding(cityBuildngs[util.getIndexFromRange(cityBuildngs.length)]);
			break;
		case "Move Rooms":
			String[] adjacientRooms = map.getPlayerAdjacientrooms();
			if(adjacientRooms.length < 1) {
				util.println("No rooms to move to");
				return;
			}
			util.println("What Room do you want to move to");
			util.printInstructionsArray(adjacientRooms);
			map.moveToRoom(adjacientRooms[util.getIndexFromRange(adjacientRooms.length)]);
			break;
		default:
			AdditionAction customCommand = findAddedCommands(command, addedCommands);
			if(customCommand == null) {
				util.println("Unknown command " + command);
			}else {
				customCommand.runAction();
				util.println(customCommand.getBaseText());
			}
		}
	}

	private AdditionAction findAddedCommands(String command, ArrayList<AdditionAction> addedCommands) {
		for(int i = 0; i < addedCommands.size(); i++) {
			if(command.equals(addedCommands.get(i).getName())) {
				return addedCommands.get(i);
			}
		}
		return null;
	}

	private void genCharacter() throws IOException, ParseException, ClassNotFound, WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, PotionNotFoundException, LocationNotFoundException {
		
		String name; 
		String className;
		do {
			myUtil.println("Hello traveler what is your name?");
			name = myUtil.getScannerString();
			myUtil.println("Is " + name + " Correct?");
		}while(!myUtil.verifyInput());
		
		className = chooseClass();
		
		player = characterFactory.generatePlayer(name, className);
		int[] baseStats = new GenerateBaseStats().generate();
		for(int i = 0; i < 6; i++) {
			switch(statOrder[i]) {
				case "str":
					player.setStr(baseStats[i]);
					break;
				case "dex":
					player.setDex(baseStats[i]);
					break;
				case "con":
					player.setCon(baseStats[i]);
					break;
				case "intel":
				case "int":
					player.setIntel(baseStats[i]);
					break;
				case "wis":
					player.setWis(baseStats[i]);
					break;
				case "cha":
					player.setCha(baseStats[i]);
					break;
			}
		}
	}

	private String chooseClass() throws IOException, ParseException {
		myUtil.println("Which class would you like to be?");
		Reader reader = new FileReader(config.getResourceFolder() + config.getClassesFile());
		JSONArray classes = (JSONArray) parser.parse(reader);
		Iterator<JSONObject> itr = classes.iterator();
		int i = 1;
		while(itr.hasNext()) {
			JSONObject curr = itr.next();
			myUtil.println("Index: " + i);
			myUtil.println((String) curr.get("name"));
			i++;
		}
		int selection = myUtil.getIndexFromRange(classes.size());
		return (String) ((JSONObject) classes.get(selection)).get("name");
	}
	
	private class generateGameStart implements Runnable{

		@Override
		public void run() {
			try {
				genActions();
				genNpc();
				genEventsAndTriggers();
			} catch (Exception e) {
				e.printStackTrace();
				gameOver = true;
			}
		}
	}
		
	
	private class GenerateBaseStats{
		int[] desicion = new int[6];
		long maxPoints;
		long currentPoints;
		ArrayList<Price> prices = new ArrayList<Price>();
		
		public int[] generate() throws IOException, ParseException {
			Reader reader = new FileReader(config.getResourceFolder() + config.getPointBuyFile());
			JSONObject pointPrices = (JSONObject) parser.parse(reader);
			maxPoints = (long) pointPrices.get("total_points");
			setUpPrices((JSONArray) pointPrices.get("prices"));
			do {
				currentPoints = maxPoints;
				this.setBaseScores();
				for(int i = 0; i < 6; i++) {
					myUtil.println("You have " + currentPoints + " points to spend");
					myUtil.println("Affordable scores");
					myUtil.println("Index | Score  |  Cost");
					int max = 0;
					for(int j = 0; j < prices.size(); j++) {
						if(prices.get(j).getCost() <= currentPoints) {
							myUtil.println((j + 1) + ((j+1) > 9 ? "" : " " ) +"    |   " + prices.get(j).getScore() +  (prices.get(j).getScore() > 9 ? "" : " " ) + "   |   " + prices.get(j).getCost());	
							max++;
						}
					}
					myUtil.println("Please select score wanted for " + statOrder[i]);
					int selection = myUtil.getIndexFromRange(max);
					currentPoints -= prices.get(selection).getCost();
					desicion[i] = prices.get(selection).getScore();
				}
				myUtil.println("Are these the scores you want?");
				for(int i = 0; i < 6; i++) {
					System.out.print(statOrder[i] + " ");
					if(i < 5) {
						System.out.print("| ");
					}else {
						myUtil.println("");
					}
				}
				for(int i = 0; i < 6; i++) {
					System.out.print(desicion[i] + " ");
					if(i < 5) {
						System.out.print((desicion[i] > 9 ? "" : " " ) + " | ");
					}else {
						myUtil.println("");
					}
				}
			}while(!myUtil.verifyInput());
			return desicion;
		}
		
		private void setUpPrices(JSONArray Jsonprices) {
			Iterator<JSONObject> itr = Jsonprices.iterator();
			while(itr.hasNext()) {
				JSONObject curr = itr.next();
				prices.add(new Price((long) curr.get("score"), (long)curr.get("cost")));
			}
		}
		
		private void setBaseScores() {
			for(int i = 0; i < 6; i++) {
				desicion[i] = 10;
			}
		}
		
		private class Price{
			private int score;
			private int cost;
			
			public Price(long l, long m) {
				score = (int) l;
				cost = (int) m;
			}

			public int getScore() {
				return score;
			}

			public int getCost() {
				return cost;
			}

		}
		
	}

	public boolean eventHasRun(String eventName) {
		for(int i = 0; i < this.triggers.size(); i++) {
			if(this.triggers.get(i).getEvent().getName().equals(eventName)) {
				if(this.triggers.get(i).getEvent().getHasRun()) {
					return true;
				}
				return false;
			}
		}
		return false;
	}
}

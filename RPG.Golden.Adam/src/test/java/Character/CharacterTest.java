package Character;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.json.simple.parser.ParseException;
import org.junit.Test;

import Items.ArmorNotFoundException;
import Items.FoodNotFoundException;
import Items.Item;
import Items.ItemFactory;
import Items.ItemNotFoundException;
import Items.PotionNotFoundException;
import Items.Shield;
import Items.UnknowItemTypeException;
import Items.Weapon;
import Items.WeaponNotFounException;
import util.my.Util;

public class CharacterTest {
	static CharacterFactory factory = CharacterFactory.getInstance();
	static ItemFactory itemFact = ItemFactory.getInstance();
	static Util myUtil = Util.getInstance();

	@Test
	public void GenCharacterTest() throws ClassNotFound, WeaponNotFounException, UnknowItemTypeException, ArmorNotFoundException, FoodNotFoundException, ItemNotFoundException, FileNotFoundException, IOException, ParseException, PotionNotFoundException {
		Player player = factory.generatePlayer("Fred", "Fighter");
		assertTrue(player.getName().equals("Fred"));
		assertTrue(player.className.equals("Fighter"));
	}

}

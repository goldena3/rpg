package util.my;

import static org.junit.Assert.*;

import org.junit.Test;

public class DiceRollerTest {
	static DiceRoller roller = DiceRoller.getInstance();

	@Test
	public void testRolls() {
		for(int i = 0; i < 100; i++) {
			int roll = roller.rollDice(1, 6); 
			assertTrue(roll > 0 && roll <= 6);
			roll = roller.rollDice(2, 20); 
			assertTrue(roll > 1 && roll <= 40);
		}
	}

}

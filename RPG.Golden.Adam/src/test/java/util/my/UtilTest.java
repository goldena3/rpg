package util.my;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import Items.Item;
import Items.Shield;
import Items.Weapon;

public class UtilTest {

	public static Util myUtil;
	
	public static Item[] inventory1 = {new Item("rope"), new Weapon("sword")};
	public static Item[] inventory2 = {new Item("rope"), new Weapon("sword"), new Item("rope")};

	@BeforeClass
	public static void getInstanceOfUtil() {
		myUtil = Util.getInstance();
		
	}
	@Test
	public void testSingltonCreation() {
		assertEquals(myUtil, Util.getInstance());
	}
	
	@Test
	public void testCalMod() {
		assertEquals(myUtil.calcMod(18), 4);
		assertEquals(myUtil.calcMod(19), 4);
		assertEquals(myUtil.calcMod(4), -3);
		assertEquals(myUtil.calcMod(3), -3);
		assertEquals(myUtil.calcMod(10), 0);
	}
	
	@Test
	public void testSearchName() {
		assertEquals(myUtil.searchItemArrayByName(inventory1, "rope"), 0);
		assertEquals(myUtil.searchItemArrayByName(inventory1, "wat"), -1);
		assertEquals(myUtil.searchItemArrayByName(inventory1, "sword"), 1);
	}
	
	@Test
	public void testSearchType() {
		Integer[] results1 = {0};
		Integer[] results2 = {1};
		Integer[] results3 = {0, 2};
		assertArrayEquals(myUtil.searchItemArrayByType(inventory1, new Item("test").getClass()), results1);
		assertArrayEquals(myUtil.searchItemArrayByType(inventory1, new Weapon("test").getClass()), results2);
		assertArrayEquals(myUtil.searchItemArrayByType(inventory1, new Shield("test").getClass()), new Integer[0]);
		assertArrayEquals(myUtil.searchItemArrayByType(inventory2, new Item("test").getClass()), results3);
	}


}
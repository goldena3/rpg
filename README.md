## Description
A framework to allow people to tell stories in an RPG style by simply writing json or by using the gui and inputing data and allowing the gui to write the json for you

## SETUP
1. import exsiting maven project in whatever IDE you use that supports maven
2. Run maven clean/update
3. In /RPG.Golden.Adam/src/main/java/util/my/ create Config.java and copy everything from Config.Example into it.
4. Set the resource folder to wehre the resource folder is on your system
	Ex: private final String resourceFolder = "C:\\Users\\"user"\\eclipse-workspace\\rpg\\RPG.Golden.Adam\\src\\main\\Resources\\";
5. (optional) Run CreateGui.java to run the GUI to add objects to the game. They are already some in
6. Run RPG/Start.java to run the main story

## Note
This is a work in progress that for now does little more then let you select your class and stats for the game
